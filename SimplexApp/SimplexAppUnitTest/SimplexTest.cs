﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OxyPlot;
using SimplexApp;
using SimplexApp.simplex_algorithm;
using System;
using System.Collections.Generic;

namespace SimplexTest {
    [TestClass]
    public class SimplexTest {
        [TestMethod]
        public void When_LinearProgramingProblemHasRealValueVariables_Expect_AddThemToEquation() {
            Simplex simplex = new Simplex(new double[,] { { 1, 1 }, { -1, 1 }, { 6, 2 } },
               new double[] { 5, 0, 21 },
               new double[] { 2, 1 },
               new Tuple<String, bool>[] { Tuple.Create("x1", true), Tuple.Create("x2", false) });
            simplex.convertToRealVariables();

            double[] expectedC = new double[] { 2, -2, 1 };
            List<String> expectedAllVariables = new List<String>();
            expectedAllVariables.Add("r1");
            expectedAllVariables.Add("r2");
            expectedAllVariables.Add("x2");
            Dictionary<String, Tuple<String, String>> expectedV = new Dictionary<String, Tuple<String, String>>();
            expectedV.Add("x1", Tuple.Create("r1", "r2"));
            expectedV.Add("x2", null);
            CollectionAssert.AreEqual(expectedC, simplex.c);
            CollectionAssert.AreEquivalent(expectedV, simplex.variablesSymbols);
            CollectionAssert.AreEquivalent(expectedAllVariables, simplex.allVariables);
        }

        [TestMethod]
        public void When_LinearProgramingProblemHasAddedRealVariables_Expect_ConvertToCanonicalForms() {
            Simplex simplex = new Simplex(new double[,] { { 1, 1 }, { -1, 1 }, { 6, 2 } },
               new double[] { 5, 0, 21 },
               new double[] { 2, 1 },
               new Tuple<String, bool>[] { Tuple.Create("x1", true), Tuple.Create("x2", false) });
            simplex.convertToRealVariables();

            double[] expectedC = new double[] { 2, -2, 1 };
            List<String> expectedAllVariables = new List<String>();
            expectedAllVariables.Add("r1");
            expectedAllVariables.Add("r2");
            expectedAllVariables.Add("x2");
            Dictionary<String, Tuple<String, String>> expectedV = new Dictionary<String, Tuple<String, String>>();
            expectedV.Add("x1", Tuple.Create("r1", "r2"));
            expectedV.Add("x2", null);
            CollectionAssert.AreEqual(expectedC, simplex.c);
            CollectionAssert.AreEquivalent(expectedV, simplex.variablesSymbols);
            CollectionAssert.AreEquivalent(expectedAllVariables, simplex.allVariables);

            simplex.convertEquationsToCanonicalForm();
            double[,] expectedCanonicalA = new double[,] { { 1, -1, 1, 1, 0, 0 }, { -1, 1, 1, 0, 1, 0 }, { 6, -6, 2, 0, 0, 1 } };
            double[] expectedCanonicalC = new double[] { 2, -2, 1, 0, 0, 0 };

            CollectionAssert.AreEqual(expectedCanonicalC, simplex.c);
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 6; j++) {
                    Assert.AreEqual(simplex.A[i, j], expectedCanonicalA[i, j], 0.0000001);
                }
            }

            expectedAllVariables.Add("y1");
            expectedAllVariables.Add("y2");
            expectedAllVariables.Add("y3");

            CollectionAssert.AreEquivalent(expectedAllVariables, simplex.allVariables);
        }

        [TestMethod]
        public void When_LinearProgramingProblemIsGivenForFirstPhaze_Expect_FirstPhaseSolve() {
            Simplex simplex = new Simplex(new double[,] { { 1, -1, 6 }, { 1, 1, 2 } },
               new double[] { 2, 1 },
               new double[] { 5, 0, 21 },
               new Tuple<String, bool>[] { Tuple.Create("x1", false), Tuple.Create("x2", false), Tuple.Create("x3", false) });
            simplex.convertToRealVariables();
            simplex.c = new double[] { -5, 0, -21, 0, 0 };
            simplex.A = new double[,] { { 1, -1, 6, -1, 0 }, { 1, 1, 2, 0, -1 } };
            simplex.allVariables.Add("y1");
            simplex.allVariables.Add("y2");
            simplex.initFirstPhase();
            List<String> expectedAllVariables = new List<String>();
            Assert.IsFalse(simplex.isSolutionOptimal());
            if (!simplex.isSolutionOptimal()) {
                simplex.elimination();
                CollectionAssert.AreEquivalent(new String[] { "x1", "x2", "y1", "y2" }, simplex.simplexTable.nonBaseVariables.Keys);
                CollectionAssert.AreEquivalent(new String[] { "x3", "z2" }, simplex.simplexTable.baseVariables.Keys);
            }

            Assert.IsFalse(simplex.isSolutionOptimal());
            if (!simplex.isSolutionOptimal()) {
                simplex.elimination();
                CollectionAssert.AreEquivalent(new String[] { "x1", "y1", "y2" }, simplex.simplexTable.nonBaseVariables.Keys);
            }

            Assert.IsTrue(simplex.isSolutionOptimal());
        }

        [TestMethod]
        public void When_LinearProgramingProblemIsGiven1_Expect_Solve() {
            Simplex simplex = new Simplex(new double[,] { { -7, -3, 0 }, { 0, -1, -2 } },
                new double[] { -2100, -1200 },
                new double[] { -0.3, -0.6, -0.2 },
                new Tuple<String, bool>[] { Tuple.Create("x1", false), Tuple.Create("x2", false), Tuple.Create("x3", false) });
            simplex.convertToRealVariables();
            simplex.convertEquationsToCanonicalForm();
            double[,] expectedCanonicalA = new double[,] { { -7, -3, 0, 1, 0 }, { 0, -1, -2, 0, 1 } };
            double[] expectedCanonicalC = new double[] { -0.3, -0.6, -0.2, 0, 0 };
            CollectionAssert.AreEquivalent(expectedCanonicalA, simplex.A);
            CollectionAssert.AreEquivalent(expectedCanonicalC, simplex.c);
            simplex.initFirstPhase();
            double[,] expectedSimplexTableValues = new double[,] {
                { -3300, -7, -4, -2, 1, 1 },
                { 0,  0.3, 0.6, 0.2 , 0, 0 },
                { 2100, 7, 3, 0, -1, 0 },
                { 1200, 0, 1, 2, 0, -1 }
            };
            CollectionAssert.AreEquivalent(expectedSimplexTableValues, simplex.simplexTable.values);
            while (!simplex.isSolutionOptimal()) {
                simplex.elimination();
            }
            CollectionAssert.AreEquivalent(new List<String>(), simplex.firstPhaseVariables);
            simplex.initSecondPhaseFromFirstPhase();
            while (!simplex.isSolutionOptimal()) {
                simplex.elimination();
            }
            Assert.AreEqual(-210, simplex.simplexTable.getTableValue(0, 0));
            Assert.AreEqual(300, simplex.simplexTable.getVariableValue("x1"));
            Assert.AreEqual(0, simplex.simplexTable.getVariableValue("x2"));
            Assert.AreEqual(600, simplex.simplexTable.getVariableValue("x3"));
            Assert.AreEqual(0, simplex.simplexTable.getVariableValue("y1"));
            Assert.AreEqual(0, simplex.simplexTable.getVariableValue("y2"));
        }

        [TestMethod]
        public void When_LinearProgramingProblemForSecondPhase_Expect_SolveProblem() {
            Simplex simplex = new Simplex(
                new double[,] { { 1, 1 }, { -1, 1 }, { 6, 2 } },
                new double[] { 5, 0, 21 },
                new double[] { 2, 1 },
                new Tuple<String, bool>[] { Tuple.Create("x1", false), Tuple.Create("x2", false) });
            simplex.convertToRealVariables();
            simplex.convertEquationsToCanonicalForm();
            simplex.initSecondPhaseFromCanonicalForm();
            CollectionAssert.AreEquivalent(new double[,] { { 0, -2, -1 }, { 5, 1, 1 }, { 0, -1, 1 }, { 21, 6, 2 } }, simplex.simplexTable.values);

            CollectionAssert.AreEquivalent(new String[] { "y1", "y2", "y3" }, simplex.simplexTable.baseVariables.Keys);
            CollectionAssert.AreEquivalent(new String[] { "x1", "x2" }, simplex.simplexTable.nonBaseVariables.Keys);

            Assert.IsFalse(simplex.isSolutionOptimal());

            while (!simplex.isSolutionOptimal()) {
                Tuple<String, String> a = simplex.findVariablesToSwap();
                simplex.elimination(a.Item1, a.Item2);
            }

            double[,] expectedSimplexTable = new double[,] {
                { 31.0/4, 1.0/4, 1.0/2 },
                { 9.0/4, -1.0/4, 3.0/2 },
                { 1.0/2, 1.0/2, -2 },
                { 11.0/4, 1.0/4, -1.0/2 } };

            for (int i = 0; i < simplex.simplexTable.tableRows; i++) {
                for (int j = 0; j < simplex.simplexTable.tableCols; j++) {
                    Assert.AreEqual(expectedSimplexTable[i, j], simplex.simplexTable.values[i, j], 0.0000001);
                }
            }

            CollectionAssert.AreEquivalent(new String[] { "x2", "y2", "x1" }, simplex.simplexTable.baseVariables.Keys);
            CollectionAssert.AreEquivalent(new String[] { "y3", "y1" }, simplex.simplexTable.nonBaseVariables.Keys);
        }


        [TestMethod]
        public void When_LinearProgramingProblemForSecondPhase_x_SolveProblem() {
            Simplex simplex = new Simplex(
               new double[,] { { -1, 1 }, { 0, -1 }},
               new double[] { 4, 2 },
               new double[] { -1, -1 },
               new Tuple<String, bool>[] { Tuple.Create("x1", true), Tuple.Create("x2", true) });
            simplex.convertToRealVariables();
            simplex.convertEquationsToCanonicalForm();
            simplex.initFirstPhase();
            while (!simplex.isSolutionOptimal()) {
                simplex.elimination();
            }

            simplex.initSecondPhaseFromFirstPhase();
            while (!simplex.isSolutionOptimal()) {
                simplex.elimination();
            }
            double x = simplex.simplexTable.getVariableValue("r1") - simplex.simplexTable.getVariableValue("r2");
            Console.WriteLine(simplex.simplexTable.getVariableValue("r1") - simplex.simplexTable.getVariableValue("r2"));
            Console.WriteLine(simplex.simplexTable.getTableValue(0, 0));
        }

        [TestMethod]
        public void When_SimplexSolverIsUser_Expect_Solve() {
            SimplexSolver simplex = new SimplexSolver(new double[,] { { -7, -3, 0 }, { 0, -1, -2 } },
                new double[] { -2100, -1200 },
                new double[] { -0.3, -0.6, -0.2 },
                new Tuple<String, bool>[] { Tuple.Create("x1", false), Tuple.Create("x2", false), Tuple.Create("x3", false) });
            Assert.AreEqual(SimplexSolver.SOLUTION_TYPE.OK, simplex.solve());

            SimplexTable result = simplex.solutions[simplex.solutions.Count - 1];
            Assert.AreEqual(-210, result.getTableValue(0, 0));
            Assert.AreEqual(300, result.getVariableValue("x1"));
            Assert.AreEqual(0, result.getVariableValue("x2"));
            Assert.AreEqual(600, result.getVariableValue("x3"));
            Assert.AreEqual(0, result.getVariableValue("y1"));
            Assert.AreEqual(0, result.getVariableValue("y2"));
        }

        [TestMethod]
        public void When_SimplexSolverIsUser_Expect_AutomaticVariableResolve() {
            SimplexSolver simplex = new SimplexSolver(new double[,] { { -7, -3, 0 }, { 0, -1, -2 } },
                new double[] { -2100, -1200 },
                new double[] { -0.3, -0.6, -0.2 },
                new Tuple<String, bool>[] { Tuple.Create("x1", false), Tuple.Create("x2", false), Tuple.Create("x3", false) });
            Assert.AreEqual(SimplexSolver.SOLUTION_TYPE.OK, simplex.solve());

            Dictionary<String, double> result = simplex.getSolutionValuesAtStep(simplex.numberOfSteps()-1);
            Assert.AreEqual(-210, result["x0"]);
            Assert.AreEqual(300, result["x1"]);
            Assert.AreEqual(0, result["x2"]);
            Assert.AreEqual(600, result["x3"]);
        }

        [TestMethod]
        public void When_OrhogonalLinesAreGiven_Expect_FindIntersectionPoint() {
            var actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(1, 10), new DataPoint(1, -10)));
            Assert.AreEqual(Tuple.Create<double, double>(1, 1), actual);
            actual = SimplexSolver.getLinesIntersection(
               Tuple.Create<DataPoint, DataPoint>(new DataPoint(1, 10), new DataPoint(1, -10)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 1)));
            Assert.AreEqual(Tuple.Create<double, double>(1, 1), actual);
        }


        [TestMethod]
        public void When_LinesDoesNotHaveIntersectionPoint_Expect_NaN() {
            var actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 5)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 2), new DataPoint(10, 6)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NegativeInfinity, Double.NegativeInfinity), actual);
            actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 5), new DataPoint(10, 5)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NegativeInfinity, Double.NaN), actual);
            actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 6), new DataPoint(10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 10), new DataPoint(10, 5)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NegativeInfinity, Double.PositiveInfinity), actual);
            actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 6), new DataPoint(-10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(10, 10), new DataPoint(10, 5)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NaN, Double.PositiveInfinity), actual);
        }

        [TestMethod]
        public void When_LinesHaveInfinityIntersectionPoint_Expect_NaN() {
            var actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 5)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 5)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NaN, Double.NaN), actual);

            actual = SimplexSolver.getLinesIntersection(
                Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 1), new DataPoint(10, 1)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NaN, Double.NaN), actual);

            actual = SimplexSolver.getLinesIntersection(
             Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 15), new DataPoint(-10, 1)), Tuple.Create<DataPoint, DataPoint>(new DataPoint(-10, 15), new DataPoint(-10, 1)));
            Assert.AreEqual(Tuple.Create<double, double>(Double.NaN, Double.NaN), actual);
        }
    }
}