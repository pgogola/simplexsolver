﻿using SimplexApp.simplex_algorithm;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SimplexApp {
    public partial class GUI : Form {
        int numberOfContraints = 0;
        int numberOfVariables = 0;
        int currentResultIndex = -1;
        private TabPage resultTabPage;
        private double[,] A;
        private double[] b;
        private double[] cvector;
        private Dictionary<string, bool> variables;
        private SimplexSolver solver = null;
        private List<ExamplePL> examplePLs = null;

        public GUI() {
            InitializeComponent();
        }

        int DropDownWidth(ComboBox myCombo) {
            int maxWidth = 0;
            int temp = 0;
            Label label1 = new Label();

            foreach (var obj in myCombo.Items) {
                label1.Text = obj.ToString();
                temp = label1.PreferredWidth;
                if (temp > maxWidth) {
                    maxWidth = temp;
                }
            }
            label1.Dispose();
            return maxWidth;
        }

        private void GUI_Load(object sender, EventArgs e) {
            constraintsAmountComboBox.SelectedIndex = 2;
            variablesAmountComboBox.SelectedIndex = 2;
            resultTabPage = windowsTabControl.TabPages[1];
            windowsTabControl.Controls.Remove(resultTabPage);

            examplePLs = new List<ExamplePL>();
            // A, b, c, realVar, var
            //examplePLs.Add(new ExamplePL(
            //    new double[,] { { 1, 1 }, { 6, 9 }, { 0, 1 } },
            //    new double[] { 100, 720, 60 },
            //    new double[] { 1, 2 },
            //    new string[] { },
            //    new string[] { "x1", "x2" },
            //    "9."
            //    ));
            //examplePLs.Add(new ExamplePL(
            //    new double[,] { { -2, 1 }, { 1, 1 }, { 5, 2 } },
            //    new double[] { 3, 6, 20 },
            //    new double[] { 1, -1 },
            //    new string[] { },
            //    new string[] { "x1", "x2" },
            //    "1. "
            //    ));
            //examplePLs.Add(new ExamplePL(
            //   new double[,] { { -2, 1 }, { 1, 1 }, { 5, 2 } },
            //   new double[] { 3, 6, 20 },
            //   new double[] { 1, 1 },
            //   new string[] { },
            //   new string[] { "x1", "x2" },
            //   "2. Inf"
            //   ));
            //examplePLs.Add(new ExamplePL(
            //   new double[,] { { 2, -1 }, { -1, 2 } },
            //   new double[] { -2, -2 },
            //   new double[] { -1, 1 },
            //   new string[] { },
            //   new string[] { "x1", "x2" },
            //   "5. X=0"
            //   ));
            //examplePLs.Add(new ExamplePL(
            //   new double[,] {
            //       { 2, -1, 89, 291, 1 },
            //       { -1, 2, 21, 21, -8 },
            //       { 0, 0, 32, -2, 2 },
            //       { 82, -32, 0, 0, 1 },
            //       { 0, 0, 1, 0, 1 } },
            //   new double[] { 2, 2, 821, 27, 1 },
            //   new double[] { 1, 1, 1, 1, 1 },
            //   new string[] { "x1", "x2", "x3", "x4", "x5" },
            //   new string[] { "x1", "x2", "x3", "x4", "x5" },
            //   "TEST"
            //   ));
            //examplePLs.Add(new ExamplePL(
            //   new double[,] {
            //       { 4, 0 },
            //       { -2, 1 } },
            //   new double[] { 10, 4 },
            //   new double[] { 2, 1},
            //   new string[] { "x1", "x2"},
            //   new string[] { "x1", "x2" },
            //   "TEST real second phase"
            //   ));
            //examplePLs.Add(new ExamplePL(
            //   new double[,] {
            //       { -1, 1, -6 },
            //       { -1, -1, -2 } },
            //   new double[] { -2, -1 },
            //   new double[] { -5, 1, -21 },
            //   new string[] { "x1", "x2", "x3" },
            //   new string[] { "x1", "x2", "x3" },
            //   "TEST real first phase"
            //   ));
            //examplePLs.Add(new ExamplePL(
            //    new double[,] { { -2, 1 }, { 1, 1 }, { 5, 2 } },
            //    new double[] { -3, 6, 20 },
            //    new double[] { 1, 1 },
            //    new string[] { "x1" },
            //    new string[] { "x1", "x2" },
            //    "x1 real"
            //    ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -2, 1 },
                    { 1, 1 },
                    { 5, 2 }
                },
                new double[] { 3, 6, 20 },
                new double[] { 1, -1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "1. Max x0=x1–x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -2, 1 },
                    { 1, 1 },
                    { 5, 2 }
                },
                new double[] { 3, 6, 20 },
                new double[] { 1, 1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "2. Max x0=x1+x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -2, 1 },
                    { 1, 1 },
                    { 5, 2 }
                },
                new double[] { 3, 6, 20 },
                new double[] { 1, 2 },
                new string[] { },
                new string[] { "x1", "x2" },
                "3. Max x0=x1+2x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -2, 1 },
                    { 1, 1 },
                    { 5, 2 }
                },
                new double[] { 3, 6, 20 },
                new double[] { -1, 1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "4. Min x0=x1–x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { 2, -1 },
                    { -1, 2 }
                },
                new double[] { -2, -2 },
                new double[] { 1, 2 },
                new string[] { },
                new string[] { "x1", "x2" },
                "5. Max x0=x1+2x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { 1, -1 },
                    { 1, 1 }
                },
                new double[] { 2, -2 },
                new double[] { -1, 2 },
                new string[] { },
                new string[] { "x1", "x2" },
                "6. Min x0=x1–x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -7, -3, 0 },
                    { -1, 0, -2 }
                },
                new double[] { -2100, -1200 },
                new double[] { -0.3, -0.6, -0.2 },
                new string[] { },
                new string[] { "x1", "x2", "x3" },
                "7. Min x0=0.3x1+0.6x2+0.2x3 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { 2, -1 },
                    { -1, 2 }
                },
                new double[] { -2, -2 },
                new double[] { 2100, 1200 },
                new string[] { },
                new string[] { "x1", "x2" },
                "8. Max x0=2100x1+1200x2 "
                ));
     
            examplePLs.Add(new ExamplePL(
                new double[,] {
                     { -1, 1 },
                     { 0, -1 }
                },
                new double[] { 4, 2 },
                new double[] { -1, -1 },
                new string[] { "x1", "x2" },
                new string[] { "x1", "x2" },
                "RV 1. Max x0=-1x1-1x2 "
                ));

            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { 1, 1 },   // x1  x2
                    { 6, 9 },
                    {0, 1 }
                },
                new double[] { 100, 720, 60 },  //ograniczenia
                new double[] { 1, 2 },
                new string[] { },
                new string[] { "x1", "x2" },
                "9. Max x0=x1+2x2 "
                ));
            examplePLs.Add(new ExamplePL(
               new double[,] {
                    { -1, 1 },   // x1  x2
                    { -1, -2 },
                    {1, 0 }
               },
               new double[] { 1, -5, 3 },  //ograniczenia
               new double[] { 1, 1 },
               new string[] { },
               new string[] { "x1", "x2" },
               "10. Max x0=x1+2x2 "
               ));
            examplePLs.Add(new ExamplePL(
               new double[,] {
                   { -1, 1 },   // x1  x2
                   { -1, -2 },
                   {1, 0}
               },
               new double[] { 1, -5, 3 },  //ograniczenia
               new double[] { -1, 4 },
               new string[] { },
               new string[] { "x1", "x2" },
               "11. Min x0=x1-4x2 "
               ));
            examplePLs.Add(new ExamplePL(
               new double[,] {
                   { -4, -1, -8, -5, -2},   // x1  x2
                   { 0, -1, -1, -2, -3}
               },
               new double[] { -12000, -18000},  //ograniczenia
               new double[] { -0.1, -0.2, -0.2 ,-0.3 ,-0.4},
               new string[] { },
               new string[] { "x1", "x2", "x3", "x4","x5" },
               "12. Min x0=0.1x1+0.2x2+0.2x3+0.3x4+0.4x5 "
               ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { 1, 2 },   // x1  x2
                    { -1, -2 },
                    {2, -1}
                },
                new double[] { 6, -2, 2 },  //ograniczenia
                new double[] { 2, -1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "13. Min x0=-2x1+x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -1, -2 },   // x1  x2
                    { 2, -1 }
                },
                new double[] { -2, 2 },  //ograniczenia
                new double[] { 2, -1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "14. Min x0=-2x1+x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -1, -2 },   // x1  x2
                    { -2, -2 },
                    {-1.5, -1.5 },
                    {-6, -4}
                },
                new double[] { -4, -6, -3, -12 },  //ograniczenia
                new double[] { -900, -1200 },
                new string[] { },
                new string[] { "x1", "x2" },
                "15. Min x0=900x1+1200x2 "
                ));
            examplePLs.Add(new ExamplePL(
                new double[,] {
                    { -1, -2 },   // x1  x2
                    { -1, 1 },
                    { 1, 0},
                    {0, -1 }
                },
                new double[] { -5, 1, 3, 0 },  //ograniczenia
                new double[] { 1, 1 },
                new string[] { },
                new string[] { "x1", "x2" },
                "16. Max x0=x1+x2 "
                ));
            prepareExamplesList();
            examplePLComboBox.DropDownWidth = DropDownWidth(examplePLComboBox);
        }

        private void solveButton_Click(object sender, EventArgs e) {
            solver = null;
            windowsTabControl.Controls.Remove(resultTabPage);
            try {
                A = new double[numberOfContraints, numberOfVariables];
                for (int r = 0; r < numberOfContraints; r++) {
                    for (int c = 0; c < numberOfVariables; c++) {
                        double result;
                        if (!Double.TryParse(matrixAForm.getElementAt(r, c).Replace(".", ","), out result)) {
                            if (matrixAForm.getElementAt(r, c) == "") {
                                throw new FormatException("Nie wprowadzono wszystkich wartości macierzy A.\nNależy wypełnić wszystkie pola.");
                            }
                            else {
                                throw new FormatException("Nieprawidłowy format wartosci macierzy A.\nPoprawny format to liczba zmiennoprzecinkowa (dozwolony separator dziesiętny: \",\" lub \".\")");
                            }
                        }
                        A[r, c] = result;
                    }
                }
                b = new double[numberOfContraints];
                for (int r = 0; r < numberOfContraints; r++) {
                    double result;
                    if (!Double.TryParse(vectorbForm.getElementAt(r, 0).Replace(".", ","), out result)) {
                        if (vectorbForm.getElementAt(r, 0) == "") {
                            throw new FormatException("Nie wprowadzono wszystkich wartości wektora b.\nNależy wypełnić wszystkie pola.");
                        }
                        else {
                            throw new FormatException("Nieprawidłowy format wartosci wektora b.\nPoprawny format to liczba zmiennoprzecinkowa (dozwolony separator dziesiętny: \",\" lub \".\")");
                        }
                    }
                    b[r] = result;
                }

                cvector = functionInputForm.getFunctionParams();

                Tuple<string, bool>[] realVar = new Tuple<string, bool>[variables.Count];
                for (int i = 0; i < realVar.Length; i++) {
                    string sym = "x" + (i + 1);
                    realVar[i] = Tuple.Create<string, bool>(sym, variables[sym]);
                }

                SimplexSolver simplexSolver = new SimplexSolver(A, b, cvector, realVar);
                switch (simplexSolver.solve()) {
                    case SimplexSolver.SOLUTION_TYPE.OK:
                        solver = simplexSolver;
                        currentResultIndex = 0;
                        MessageBox.Show("Znaleziono rozwiązanie optymalne dla podanego zadania PL.");
                        windowsTabControl.Controls.Add(resultTabPage);
                        updateResultTab();
                        break;
                    case SimplexSolver.SOLUTION_TYPE.MANY_SOLUTIONS:
                        solver = simplexSolver;
                        currentResultIndex = 0;
                        MessageBox.Show("Podane zadanie PL posiada więcej niż jedno rozwiązanie optymalne.");
                        windowsTabControl.Controls.Add(resultTabPage);
                        updateResultTab();
                        break;
                    case SimplexSolver.SOLUTION_TYPE.CONFLICT:
                        MessageBox.Show("Podane zadanie PL nie ma rozwiązania dopuszczalnego - brak rozwiązania optymalnego.");
                        windowsTabControl.Controls.Add(resultTabPage);
                        updateResultTab();
                        break;
                    case SimplexSolver.SOLUTION_TYPE.UNBOUNDED:
                        MessageBox.Show("Podane zadanie PL jest nieograniczone - brak rozwiązania optymalnego.");
                        windowsTabControl.Controls.Add(resultTabPage);
                        updateResultTab();
                        break;
                    default:
                        throw new NotImplementedException("Unrecognized UserValidationResult value.");
                }
            }
            catch (FormatException ex) {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex) {
                MessageBox.Show("Wystąpił nieoczekiwany wyjątek:\n" + ex.Message);
            }
        }

        private void constraintsAmountComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            numberOfContraints = Convert.ToInt16(constraintsAmountComboBox.Items[constraintsAmountComboBox.SelectedIndex].ToString());
            matrixAForm.updateMatrixSize(numberOfContraints, numberOfVariables);
            vectorbForm.updateMatrixSize(numberOfContraints, 1);
        }

        private void variablesAmountComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            inputDataTableLayout.SuspendLayout();
            numberOfVariables = Convert.ToInt16(variablesAmountComboBox.Items[variablesAmountComboBox.SelectedIndex].ToString());
            matrixAForm.updateMatrixSize(numberOfContraints, numberOfVariables);
            String[,] values = new string[numberOfVariables, 1];
            selectRealVariablesListBox.ClearSelected();
            selectRealVariablesListBox.Items.Clear();
            variables = new Dictionary<string, bool>();
            for (int i = 0; i < numberOfVariables; i++) {
                values[i, 0] = "x" + (i + 1);
                selectRealVariablesListBox.Items.Add("x" + (i + 1));
                variables.Add("x" + (i + 1), false);
            }
            matrixXForm.updateMatrixSize(numberOfVariables, 1, values);
            selectRealVariablesListBox_ItemCheck(null, null);
            functionInputForm.updateFunctionForm(numberOfVariables);
            inputDataTableLayout.ResumeLayout();
        }

        private void selectRealVariablesListBox_ItemCheck(object sender, ItemCheckEventArgs e) {
            string realVariables = "";
            string nonRealVariables = "";

            if (null != e) {
                variables[selectRealVariablesListBox.Items[e.Index].ToString()] = (CheckState.Checked == e.NewValue);
            }
            foreach (var elem in variables) {
                if (elem.Value) {
                    realVariables += elem.Key + " ";
                }
                else {
                    nonRealVariables += elem.Key + " ";
                }
            }
            if (realVariables != "") {
                realVariables += "\u2208 R";
            }
            if (nonRealVariables != "") {
                nonRealVariables += "\u2265 0";
            }
            realVariablesLabel.Text = realVariables;
            greaterXContraintLabel.Text = nonRealVariables;
        }

        private void resultFirstButton_Click(object sender, EventArgs e) {
            currentResultIndex = 0;
            updateResultTab();
        }

        private void resultPreviousButton_Click(object sender, EventArgs e) {
            if (currentResultIndex > 0) {
                currentResultIndex--;
                updateResultTab();
            }
        }

        private void resultNextButton_Click(object sender, EventArgs e) {
            if (currentResultIndex < solver.solutions.Count - 1) {
                currentResultIndex++;
                updateResultTab();
            }
        }

        private void resultLastButton_Click(object sender, EventArgs e) {
            currentResultIndex = solver.solutions.Count - 1;
            updateResultTab();
        }

        private void updateResultTab() {
            string fun = "f(x) = ";
            double[] p = functionInputForm.getFunctionParams();
            for (int i = 0; i < p.Length; i++) {
                fun += p[i] + "x" + (i + 1) + (i < p.Length - 1 ? " + " : "");
            }
            functionRememberLabel.Text = fun;
            if (solver != null) {
                resultIndexBox.Text = currentResultIndex.ToString();
                resultSimplexTableForm.updateSimplexTable(solver.getTableAtStep(currentResultIndex));
                currentResultLabel.Text = "";
                foreach (var v in solver.getSolutionValuesAtStep(currentResultIndex)) {
                    currentResultLabel.Text += v.Key + "=" + v.Value + "\n";
                }
            }
            Console.WriteLine(currentResultLabel.Text);
            if (null == solver) {
                showResultLayoutPanel.ColumnStyles[0].SizeType = SizeType.Absolute;
                showResultLayoutPanel.ColumnStyles[0].Width = 0;
                showResultLayoutPanel.ColumnStyles[1].SizeType = SizeType.Percent;
                showResultLayoutPanel.ColumnStyles[1].Width = 100;
                plotForm.initializePlot("Warstwica", A, b, cvector, solver, currentResultIndex, variables["x1"], variables["x2"]);
            }
            else if (cvector.Length == 2) {
                showResultLayoutPanel.ColumnStyles[1].SizeType = SizeType.Percent;
                showResultLayoutPanel.ColumnStyles[1].Width = 100;
                showResultLayoutPanel.ColumnStyles[0].SizeType = SizeType.AutoSize;
                //showResultLayoutPanel.ColumnStyles[1].Width = 100;
                plotForm.initializePlot("Warstwica", A, b, cvector, solver, currentResultIndex, variables["x1"], variables["x2"]);
            }
            else {
                showResultLayoutPanel.ColumnStyles[1].SizeType = SizeType.Percent;
                showResultLayoutPanel.ColumnStyles[1].Width = 0;
                showResultLayoutPanel.ColumnStyles[0].SizeType = SizeType.Percent;
                showResultLayoutPanel.ColumnStyles[0].Width = 100;
            }
        }

        private void resultIndexBox_TextChanged(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                int newIndex = Convert.ToInt32(resultIndexBox.Text);
                if (newIndex >= solver.solutions.Count || newIndex < 0) {
                    return;
                }
                currentResultIndex = newIndex;
                resultSimplexTableForm.updateSimplexTable(solver.getTableAtStep(currentResultIndex));
            }
        }


        void prepareExamplesList() {
            foreach (var example in examplePLs) {
                examplePLComboBox.Items.Add(example.getDescription() + " " + example.getFunction() + "; n=" + example.getNumberOfVariables() + "; m=" + example.getNumberOfContraints() + "; " + example.getNonRealVariablesString() + "; " + example.getRealVariablesString());
            }
            examplePLComboBox.SelectedIndex = 0;
        }

        private void examplePLAcceptButton_Click(object sender, EventArgs e) {
            ExamplePL exPL = examplePLs[examplePLComboBox.SelectedIndex];
            constraintsAmountComboBox.Text = exPL.getNumberOfContraints().ToString();
            variablesAmountComboBox.Text = exPL.getNumberOfVariables().ToString();
            for (int r = 0; r < exPL.getNumberOfContraints(); r++) {
                for (int c = 0; c < exPL.getNumberOfVariables(); c++) {
                    matrixAForm.setElementAt(exPL.A[r, c].ToString(), r, c);
                }
            }
            for (int r = 0; r < exPL.getNumberOfContraints(); r++) {
                vectorbForm.setElementAt(exPL.b[r].ToString(), r, 0);
            }
            functionInputForm.setFunctionParams(exPL.c);

            foreach (int itch in selectRealVariablesListBox.CheckedIndices) {
                selectRealVariablesListBox.SetItemChecked(itch, false);
            }
            foreach (var v in exPL.realVariables) {
                int index = selectRealVariablesListBox.Items.IndexOf(v);
                selectRealVariablesListBox.SetItemChecked(index, true);
            }
        }
    }
}
