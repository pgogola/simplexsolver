﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplexApp.simplex_algorithm {
    public class SimplexSolver {
        static public Tuple<double, double> getLinesIntersection(Tuple<DataPoint, DataPoint> p1, Tuple<DataPoint, DataPoint> p2) {
            //https://en.wikipedia.org/wiki/Line–line_intersection

            double x1 = p1.Item1.X;
            double y1 = p1.Item1.Y;
            double x2 = p1.Item2.X;
            double y2 = p1.Item2.Y;

            double x3 = p2.Item1.X;
            double y3 = p2.Item1.Y;
            double x4 = p2.Item2.X;
            double y4 = p2.Item2.Y;

            double Px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

            double Py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
            return Tuple.Create<double, double>(Px, Py);
        }

        private Simplex simplex;

        public List<SimplexTable> solutions;

        public enum SOLUTION_TYPE { OK, UNBOUNDED, CONFLICT, MANY_SOLUTIONS}

        public SimplexSolver(double[,] A, double[] b, double[] c, Tuple<String, bool>[] variables) {
            simplex = new Simplex(A, b, c, variables);
            solutions = new List<SimplexTable>();
        }

        public SOLUTION_TYPE solve() {
            simplex.convertToRealVariables();
            simplex.convertEquationsToCanonicalForm();
            if (!simplex.isFeasibleBasic()) {
                simplex.initFirstPhase();
                solutions.Add(simplex.simplexTable.clone());
                while (!simplex.isSolutionOptimal()) {
                    Tuple<string, string> vars = simplex.findVariablesToSwap();
                    if(vars.Item2 == null) {
                        return SOLUTION_TYPE.UNBOUNDED;
                    }
                    simplex.elimination();
                    solutions.Add(simplex.simplexTable.clone());
                }
                simplex.initSecondPhaseFromFirstPhase();
                if (simplex.isFeasibleBasic()) {
                    return SOLUTION_TYPE.CONFLICT;
                }
            }
            else {
                simplex.initSecondPhaseFromCanonicalForm();
            }
            solutions.Add(simplex.simplexTable.clone());
            while (!simplex.isSolutionOptimal()) {
                Tuple<string, string> vars = simplex.findVariablesToSwap();
                if (vars.Item2 == null) {
                    return SOLUTION_TYPE.UNBOUNDED;
                }
                simplex.elimination();
                solutions.Add(simplex.simplexTable.clone());
            }
            if (simplex.hasMoreThanOneOptimalSolutions()) {
                return SOLUTION_TYPE.MANY_SOLUTIONS;
            }
            return SOLUTION_TYPE.OK;
        }

        public int numberOfSteps() {
            return this.solutions.Count;
        }

        public SimplexTable getTableAtStep(int step) {
            return solutions[step];
        }

        public Dictionary<String, double> getSolutionValuesAtStep(int step) {
            SimplexTable simplexTable = solutions[step];
            Dictionary<String, double> result = new Dictionary<string, double>();
            if (simplexTable.phase == SimplexTable.Phase.FIRST_PHASE) {
                result.Add("x0", simplexTable.getTableValue(1, 0));
                result.Add("z0", simplexTable.getTableValue(0, 0));
            }
            else {
                result.Add("x0", simplexTable.getTableValue(0, 0));
            }
            foreach (KeyValuePair<String, Tuple<String, String>> entry in simplex.variablesSymbols) {
                if(null == entry.Value) {
                    result.Add(entry.Key, simplexTable.getVariableValue(entry.Key));
                }
                else {
                    result.Add(entry.Key, simplexTable.getVariableValue(entry.Value.Item1) - simplexTable.getVariableValue(entry.Value.Item2));
                }
            }
            return result;
        }
    }
}
