﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SimplexApp.simplex_algorithm {
    [Serializable]
    public class SimplexTable {

        public enum Phase { FIRST_PHASE, SECOND_PHASE};

        public Phase phase;
        public int baseVariablesAmount { get; set; } //number of base variables 
        public int tableRows;

        public int nonBaseVariablesAmount { get; } //number of non base variables 
        public int tableCols;
        public double[,] values { get; }

        public Dictionary<String, int> baseVariables;
        public Dictionary<String, int> nonBaseVariables;

        public SimplexTable(int baseVariablesAmount, int nonBaseVariablesAmount) {
            this.baseVariablesAmount = baseVariablesAmount;
            this.nonBaseVariablesAmount = nonBaseVariablesAmount;
            tableRows = baseVariablesAmount + 1;
            tableCols = nonBaseVariablesAmount + 1;
            values = new double[baseVariablesAmount + 1, nonBaseVariablesAmount + 1];
            baseVariables = new Dictionary<string, int>();
            nonBaseVariables = new Dictionary<string, int>();
        }

        public void setTableValue(int row, int col, double value) {
            values[row, col] = value;
        }

        public double getTableValue(int row, int col) {
            return values[row, col];
        }

        public SimplexTable clone() {
            using (MemoryStream stream = new MemoryStream()) {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                return (SimplexTable)formatter.Deserialize(stream);
            };
        }

        public int getNonBaseVariableColumnIndex(String key) {
            int value = 0;
            nonBaseVariables.TryGetValue(key, out value);
            return value;
        }

        public int getBaseVariableRowIndex(String key) {
            int value = 0;
            baseVariables.TryGetValue(key, out value);
            return value;
        }


        public double getVariableValue(String key) {
            int index = 0;
            if (baseVariables.TryGetValue(key, out index)) {
                return values[index, 0];
            }
            return 0;
        }
    }
}
