﻿using System;
using System.Collections.Generic;

namespace SimplexApp.simplex_algorithm {
    public class Simplex {
        public double[,] originalA;
        public double[] originalc;
        public double[] originalb;

        public double[,] A;
        public double[] c;
        public double[] b;
        public Tuple<String, bool>[] variables; //if true then variable is real
        public SimplexTable simplexTable; //current simplex table

        public Dictionary<String, Tuple<String, String>> variablesSymbols; //variable symbols map from given to real
        public List<String> firstPhaseVariables; //artificial variables in first phase
        public int realVariablesNum; //number of real variables (-inf , inf)
        public int nonRealVariablesNum; //number of varaibles >0

        public List<String> allVariables;
        public List<String> artificialVariables;

        public Simplex(double[,] A, double[] b, double[] c, Tuple<String, bool>[] variables) {
            originalA = A;
            originalb = b;
            originalc = c;
            this.variables = variables;
        }

        //convert real value variables to 2 additional variables
        public void convertToRealVariables() {
            nonRealVariablesNum = 0;
            realVariablesNum = 0;
            foreach (Tuple<String, bool> v in variables) {
                if (v.Item2) {
                    realVariablesNum+=2;
                }
                else {
                    nonRealVariablesNum++;
                }
            }
            int m = originalb.Length;
            int n = nonRealVariablesNum + 2 * realVariablesNum;
            double[] cTmp = new double[n];
            double[] bTmp = originalb;
            double[,] aTmp = new double[m, n];
            allVariables = new List<String>();
            variablesSymbols = new Dictionary<string, Tuple<string, string>>();

            int idxTmp = 0;
            int idxReal = 0;
            int realVariableIndex = 1;
            foreach (Tuple<String, bool> v in variables) {
                cTmp[idxTmp] = originalc[idxReal];
                for (int i = 0; i < m; i++) {
                    aTmp[i, idxTmp] = originalA[i, idxReal];
                }
                if (v.Item2) {
                    idxTmp++;
                    cTmp[idxTmp] = -originalc[idxReal];
                    for (int i = 0; i < m; i++) {
                        aTmp[i, idxTmp] = -originalA[i, idxReal];
                    }
                    variablesSymbols.Add(v.Item1, Tuple.Create("r" + realVariableIndex, "r" + (realVariableIndex + 1)));
                    allVariables.Add("r" + realVariableIndex);
                    allVariables.Add("r" + (realVariableIndex + 1));
                    realVariableIndex += 2;
                }
                else {
                    allVariables.Add(v.Item1);
                    variablesSymbols.Add(v.Item1, null);
                }
                idxTmp++;
                idxReal++;
            }

            c = cTmp;
            b = bTmp;
            A = aTmp;
        }

        public void convertEquationsToCanonicalForm() {
            int m = b.Length;
            int n = c.Length + b.Length;
            double[,] tmpA = new double[m, n];
            double[] tmpc = new double[n];
            Array.Copy(c, tmpc, c.Length);
            for (int i = c.Length; i < n; i++) {
                tmpc[i] = 0;
            }

            for (int row = 0; row < m; row++) {
                for (int col = 0; col < c.Length; col++) {
                    tmpA[row, col] = A[row, col];
                }
            }

            artificialVariables = new List<string>();
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n - c.Length; j++) {
                    tmpA[i, c.Length + j] = i == j ? 1 : 0;
                }
                allVariables.Add("y" + (i + 1));
                artificialVariables.Add("y" + (i + 1));
            }

            A = tmpA;
            c = tmpc;
        }

        //first phase
        public Tuple<String, String> findVariablesToSwap() {
            String nonBaseVariable = findVariableToRemoveFromNonBaseFirstPhase();
            return new Tuple<String, String>(nonBaseVariable, findVariableToRemoveFromBaseFirstPhase(nonBaseVariable));
        }

        private String findVariableToRemoveFromNonBaseFirstPhase() {
            String nonBaseVariable = null;
            double minimum = double.PositiveInfinity;
            foreach (var v in simplexTable.nonBaseVariables) {
                if (simplexTable.getTableValue(0, v.Value) < minimum) {
                    minimum = simplexTable.getTableValue(0, v.Value);
                    nonBaseVariable = v.Key;
                }
            }
            return nonBaseVariable;
        }

        private String findVariableToRemoveFromBaseFirstPhase(String variableToRemoveFromNonBase) {
            String baseVariable = null;
            double minimum = double.PositiveInfinity;
            int indexColumnOfVariableToRemoveFromNonBase = 0;
            simplexTable.nonBaseVariables.TryGetValue(variableToRemoveFromNonBase, out indexColumnOfVariableToRemoveFromNonBase);

            foreach (var v in simplexTable.baseVariables) {
                if (simplexTable.getTableValue(v.Value, indexColumnOfVariableToRemoveFromNonBase) > 0 &&
                    simplexTable.getTableValue(v.Value, 0) / simplexTable.getTableValue(v.Value, indexColumnOfVariableToRemoveFromNonBase) < minimum) {
                    minimum = simplexTable.getTableValue(v.Value, 0) / simplexTable.getTableValue(v.Value, indexColumnOfVariableToRemoveFromNonBase);
                    baseVariable = v.Key;
                }
            }
            return baseVariable;
        }

        public void initFirstPhase() {
            firstPhaseVariables = new List<string>();
            simplexTable = new SimplexTable(b.Length + 1, c.Length);
            simplexTable.phase = SimplexTable.Phase.FIRST_PHASE;

            int nonBaseVariableColumn = 1;
            foreach (String v in allVariables) {
                simplexTable.nonBaseVariables.Add(v, nonBaseVariableColumn++);
            }
            for (int row = 0; row < b.Length; row++) {
                if (b[row] > 0) {
                    simplexTable.setTableValue(row + 2, 0, b[row]);
                    for (int col = 0; col < c.Length; col++) {
                        simplexTable.setTableValue(row + 2, col + 1, A[row, col]);
                    }
                }
                else {
                    simplexTable.setTableValue(row + 2, 0, -b[row]);
                    for (int col = 0; col < c.Length; col++) {
                        simplexTable.setTableValue(row + 2, col + 1, -A[row, col]);
                    }
                }
            }
            simplexTable.setTableValue(1, 0, 0);
            for (int col = 0; col < c.Length; col++) {
                simplexTable.setTableValue(1, col + 1, -c[col]);
            }
            for (int col = 0; col < simplexTable.tableCols; col++) {
                double sum = 0;
                for (int row = 2; row < simplexTable.tableRows; row++) {
                    sum += simplexTable.getTableValue(row, col);
                }
                simplexTable.setTableValue(0, col, -sum);
            }
            //simplexTable.baseVariables.Add("x0", 1);
            for (int i = 1; i < simplexTable.baseVariablesAmount; i++) {
                simplexTable.baseVariables.Add("z" + i, i + 1);
                firstPhaseVariables.Add("z" + i);
            }
        }

        public void elimination() {
            Tuple<String, String> indeces = findVariablesToSwap();
            elimination(indeces.Item1, indeces.Item2);
            if (firstPhaseVariables.Contains(indeces.Item2)) {
                firstPhaseVariables.Remove(indeces.Item2);
                SimplexTable simplexT = new SimplexTable(simplexTable.baseVariablesAmount, simplexTable.nonBaseVariablesAmount - 1);
                simplexT.baseVariables = simplexTable.baseVariables;


                for (int row = 0; row < simplexT.tableRows; row++) {
                    simplexT.values[row, 0] = simplexTable.values[row, 0];
                }

                int indexOfRemovedVariable = simplexTable.getNonBaseVariableColumnIndex(indeces.Item2);
                foreach (var v in simplexTable.nonBaseVariables) {
                    if (v.Value > indexOfRemovedVariable) {
                        for (int row = 0; row < simplexT.tableRows; row++) {
                            simplexT.values[row, v.Value - 1] = simplexTable.values[row, v.Value];
                        }
                        simplexT.nonBaseVariables.Add(v.Key, v.Value - 1);
                    }
                    else if (v.Value < indexOfRemovedVariable) {
                        for (int row = 0; row < simplexT.tableRows; row++) {
                            simplexT.values[row, v.Value] = simplexTable.values[row, v.Value];
                        }
                        simplexT.nonBaseVariables.Add(v.Key, v.Value);
                    }
                }
                simplexTable = simplexT;
            }
        }


        //second phase
        public void initSecondPhaseFromFirstPhase() {
            SimplexTable simplexT = new SimplexTable(simplexTable.baseVariablesAmount - 1, simplexTable.nonBaseVariablesAmount);

            for (int row = 0; row < simplexT.tableRows; row++) {
                simplexT.values[row, 0] = simplexTable.values[row + 1, 0];
            }

            foreach (var v in simplexTable.baseVariables) {
                simplexT.baseVariables.Add(v.Key, v.Value - 1);
            }

            foreach (var v in simplexTable.nonBaseVariables) {
                for (int row = 0; row < simplexT.tableRows; row++) {
                    simplexT.values[row, v.Value] = simplexTable.values[row + 1, v.Value];
                }
            }
            simplexT.nonBaseVariables = simplexTable.nonBaseVariables;
            simplexTable = simplexT;
            simplexT.phase = SimplexTable.Phase.SECOND_PHASE;
        }

        public void initSecondPhaseFromCanonicalForm() {
            firstPhaseVariables = new List<string>();

            simplexTable = new SimplexTable(artificialVariables.Count, nonRealVariablesNum + realVariablesNum);

            simplexTable.setTableValue(0, 0, 0);

            for (int row = 1; row < simplexTable.tableRows; row++) {
                simplexTable.values[row, 0] = b[row - 1];
                simplexTable.baseVariables.Add(artificialVariables[row - 1], row);
                for (int col = 1; col < simplexTable.tableCols; col++) {
                    simplexTable.setTableValue(row, col, A[row - 1, col - 1]);
                }
            }

            for (int col = 1; col < simplexTable.tableCols; col++) {
                simplexTable.nonBaseVariables.Add(allVariables[col - 1], col);
                simplexTable.setTableValue(0, col, -c[col - 1]);
            }
            simplexTable.phase = SimplexTable.Phase.SECOND_PHASE;
        }

        //elimination for both phases
        public void elimination(String nonBaseVariable, String baseVariable) {

            double yrk = simplexTable.getTableValue(simplexTable.getBaseVariableRowIndex(baseVariable), simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable));
            for (int col = 0; col < simplexTable.tableCols; col++) { // pkt. 1
                if (col != simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable)) {
                    simplexTable.setTableValue(simplexTable.getBaseVariableRowIndex(baseVariable), col, simplexTable.getTableValue(simplexTable.getBaseVariableRowIndex(baseVariable), col) / yrk);
                }
            }

            for (int row = 0; row < simplexTable.tableRows; row++) { // pkt. 2
                if (row != simplexTable.getBaseVariableRowIndex(baseVariable)) {
                    double yik = simplexTable.getTableValue(row, simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable));
                    for (int col = 0; col < simplexTable.tableCols; col++) {
                        if (col != simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable)) {
                            simplexTable.setTableValue(row, col, simplexTable.getTableValue(row, col) - simplexTable.getTableValue(simplexTable.getBaseVariableRowIndex(baseVariable), col) * yik);
                        }
                    }
                }
            }

            for (int row = 0; row < simplexTable.tableRows; row++) { // pkt. 3
                if (row == simplexTable.getBaseVariableRowIndex(baseVariable)) {
                    simplexTable.setTableValue(row, simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable), 1.0 / yrk);
                }
                else {
                    simplexTable.setTableValue(row, simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable), -simplexTable.getTableValue(row, simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable)) / yrk);
                }
            }

            //swap varriable names (non base and base variable)
            int tmpNonBase = simplexTable.getNonBaseVariableColumnIndex(nonBaseVariable);
            int tmpBase = simplexTable.getBaseVariableRowIndex(baseVariable);

            simplexTable.nonBaseVariables.Remove(nonBaseVariable);
            simplexTable.nonBaseVariables.Add(baseVariable, tmpNonBase);

            simplexTable.baseVariables.Remove(baseVariable);
            simplexTable.baseVariables.Add(nonBaseVariable, tmpBase);
        }

        public bool isFeasibleBasic() {
            foreach (int v in b) {
                if (v < 0) {
                    return false;
                }
            }
            return true;
        }

        public bool hasMoreThanOneOptimalSolutions() {
            for (int i = 1; i < simplexTable.tableRows; i++) {
                if (simplexTable.getTableValue(i, 0) < 0) {
                    return false;
                }
            }
            for (int j = 1; j < simplexTable.tableCols; j++) {
                if (simplexTable.getTableValue(0, j) < 0) {
                    return false;
                }
                else if (simplexTable.getTableValue(0, j) == 0) {
                    for (int i = 1; i < simplexTable.tableRows; i++) {
                        if(simplexTable.getTableValue(i, 0) > 0 && simplexTable.getTableValue(i, j) > 0) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool isSolutionOptimal() {
            foreach (var v in simplexTable.nonBaseVariables) {
                if (simplexTable.getTableValue(0, v.Value) < 0) {
                    return false;
                }
            }
            return true;
        }

    }

}
