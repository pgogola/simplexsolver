﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SimplexApp.simplex_algorithm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace SimplexApp {
    public partial class PlotForm : UserControl {

        double scaleAxisRatioDraw = 0.5;
        double scaleAxisRatioAxis = 0.1;

        private class ConstraintFunction {
            int idx;
            double[,] A;
            double[] b;
            public ConstraintFunction(double[,] A, double[] b, int i) {
                this.A = A;
                this.b = b;
                idx = i;
            }
            public double Lambda(double x1) {
                return divide(b[idx] - A[idx, 0] * x1, A[idx, 1]);
            }
        }

        static Tuple<DataPoint, DataPoint> getTwoPointsOnLine(double[] A, double b, double minx1 = -10.0, double maxx1 = 10, double minx2 = -10, double maxx2 = 10) {
            Tuple<DataPoint, DataPoint> points = null;
            if (A[0] == 0) { //const function - horizontal line
                points = Tuple.Create(new DataPoint(minx1, b / A[1]), new DataPoint(maxx1, b / A[1]));
            }
            else if (A[1] == 0) { //vertical line
                points = Tuple.Create(new DataPoint(b / A[0], minx2), new DataPoint(b / A[0], maxx2));
            }
            else { //linear function
                points = Tuple.Create(new DataPoint(minx1, (b - A[0] * (minx1)) / A[1]), new DataPoint(maxx1, (b - A[0] * (maxx1)) / A[1]));
            }
            return points;
        }

        static Tuple<double, double> getIntersectionPoinOfTwoFun(double[] A1, double b1, double[] A2, double b2) {
            var result = SimplexSolver.getLinesIntersection(getTwoPointsOnLine(A1, b1), getTwoPointsOnLine(A2, b2));
            if (Double.IsInfinity(result.Item1) || Double.IsInfinity(result.Item2) || Double.IsNaN(result.Item1) || Double.IsNaN(result.Item2)) {
                return null;
            }
            return result;
        }

        static Func<double, double, double> divide = (arg1, arg2) => { return arg2 == 0 ? arg1 : arg1 / arg2; };

        public PlotForm() {
            InitializeComponent();
        }

        public void initializePlot(string title, double[,] A, double[] b, double[] c,
SimplexSolver simplexTables, int currentStep, bool isx1real, bool isx2real) {
            Debug.Assert(A.GetLength(0) == b.Length);
            Debug.Assert(A.GetLength(1) == c.Length);

            List<DataPoint> areaDataPoints = constraintsPoints(A, b, isx1real, isx2real);

            var plotModel = new PlotModel {
                Title = title
            };

            double maxx1 = Double.NegativeInfinity;
            double maxx2 = Double.NegativeInfinity;
            double minx1 = Double.PositiveInfinity;
            double minx2 = Double.PositiveInfinity;
            foreach (var p in areaDataPoints) {
                maxx1 = Math.Max(p.X, maxx1);
                maxx2 = Math.Max(p.Y, maxx2);
                minx1 = Math.Min(p.X, minx1);
                minx2 = Math.Min(p.Y, minx2);
            }

            if (areaDataPoints.Count < 2) {
                maxx1 = 10;
                maxx2 = 10;
                minx1 = -10;
                minx2 = -10;
            }
            plotModel.Axes.Add(new LinearColorAxis {
                Palette = OxyPalettes.Rainbow(100),
                Maximum = maxx1 + (maxx1 - minx1) * scaleAxisRatioAxis,
                Minimum = minx1 - (maxx1 - minx1) * scaleAxisRatioAxis
            });

            plotModel.Axes.Add(new LinearAxis {
                Position = AxisPosition.Bottom,
                Title = "x1",
                Maximum = maxx1 + (maxx1 - minx1) * scaleAxisRatioAxis,
                Minimum = minx1 - (maxx1 - minx1) * scaleAxisRatioAxis,
                IsZoomEnabled = true,
                IsAxisVisible = true
            });
            plotModel.Axes.Add(new LinearAxis {
                Position = AxisPosition.Left,
                Title = "x2",
                Maximum = maxx2 + (maxx2 - minx2) * scaleAxisRatioAxis,
                Minimum = minx2 - (maxx2 - minx2) * scaleAxisRatioAxis,
                IsZoomEnabled = true,
                IsAxisVisible = true
            });

            plotModel.Series.Add(createHeatMapSeries(c, minx1, maxx1, minx2, maxx2));
            if (simplexTables != null) {
                foreach(var points in createPointsScatterSeries(simplexTables, currentStep))
                plotModel.Series.Add(points);
            }
            foreach (var fs in creteFunctionsSeries(A, b, c, isx1real, isx2real)) {
                plotModel.Series.Add(fs);
            }
            if (areaDataPoints.Count > 1) {
                plotModel.Series.Add(createAreaSeries(areaDataPoints));
            }
            plotView.Model = plotModel;
        }

        List<LineSeries> creteFunctionsSeries(double[,] A, double[] b, double[] c, bool isx1real, bool isx2real) {
            var functionsSeries = new List<LineSeries>();
            for (int i = 0; i < b.Length; i++) {
                var ls = new LineSeries {
                    Color = OxyColors.Black,
                    MarkerFill = OxyColors.Transparent,
                };
                var points = getTwoPointsOnLine(new double[] { A[i, 0], A[i, 1] }, b[i]);
                ls.Points.Add(points.Item1);
                ls.Points.Add(points.Item2);
                functionsSeries.Add(ls);
            }

            if (!isx1real) {
                var ls = new LineSeries {
                    Color = OxyColors.White,
                    MarkerFill = OxyColors.Transparent,
                };
                var points = getTwoPointsOnLine(new double[] { 1, 0 }, 0);
                ls.Points.Add(points.Item1);
                ls.Points.Add(points.Item2);
                functionsSeries.Add(ls);
            }
            if (!isx2real) {
                var ls = new LineSeries {
                    Color = OxyColors.White,
                    MarkerFill = OxyColors.Transparent,
                };
                var points = getTwoPointsOnLine(new double[] { 0, 1 }, 0);
                ls.Points.Add(points.Item1);
                ls.Points.Add(points.Item2);
                functionsSeries.Add(ls);
            }


            return functionsSeries;
        }

        HeatMapSeries createHeatMapSeries(double[] c, double minx1, double maxx1, double minx2, double maxx2) {
            double x1minAreaBound = minx1 - (maxx1 - minx1) * scaleAxisRatioDraw;
            double x1maxAreaBound = maxx1 + (maxx1 - minx1) * scaleAxisRatioDraw;
            double x2minAreaBound = minx2 - (maxx2 - minx2) * scaleAxisRatioDraw;
            double x2maxAreaBound = maxx2 + (maxx2 - minx2) * scaleAxisRatioDraw;

            double step = Math.Max((x1maxAreaBound - x1minAreaBound) / 1000, (x2maxAreaBound - x2minAreaBound) / 1000);


            int x1size = (int)((x1maxAreaBound - x1minAreaBound) / step);
            int x2size = (int)((x2maxAreaBound - x2minAreaBound) / step);
            double[,] f_x = new double[x1size, x2size];
            double x1 = x1minAreaBound;
            for (int i = 0; i < x1size; i++) {
                double x2 = x2minAreaBound;
                for (int j = 0; j < x2size; j++) {
                    f_x[i, j] = c[0] * x1 + c[1] * x2;
                    x2 += step;
                }
                x1 += step;
            }
            return new HeatMapSeries {
                X0 = x1minAreaBound,
                X1 = x1maxAreaBound,
                Y0 = x2minAreaBound,
                Y1 = x2maxAreaBound,
                Interpolate = true,
                RenderMethod = HeatMapRenderMethod.Bitmap,
                Data = f_x
            };
        }

        List<LineSeries> createPointsScatterSeries(SimplexSolver simplexTables, int currentStep) {

            var lineSeries1 = new LineSeries {
                MarkerType = MarkerType.Circle,
                MarkerFill = OxyColors.Azure,
                MarkerSize = 4
            };

            for (int i = 0; i < currentStep; i++) {
                var values1 = simplexTables.getSolutionValuesAtStep(i);
                lineSeries1.Points.Add(new DataPoint(values1["x1"], values1["x2"]));
            }
            var lineSeries2 = new LineSeries {
                MarkerType = MarkerType.Circle,
                MarkerFill = OxyColors.Black,
                MarkerSize = 5
            };

            var values = simplexTables.getSolutionValuesAtStep(currentStep);
            lineSeries2.Points.Add(new DataPoint(values["x1"], values["x2"]));


            var lineSeries = new List<LineSeries>();
            lineSeries.Add(lineSeries1);
            lineSeries.Add(lineSeries2);
            return lineSeries;
        }

        List<DataPoint> constraintsPoints(double[,] A, double[] b, bool isx1real, bool isx2real) {
            Func<double[,], int, double[]> getRow = (arr, idx) => {
                double[] r = new double[arr.GetLength(1)];
                for (int i = 0; i < r.Length; i++) {
                    r[i] = arr[idx, i];
                }
                return r;
            };

            List<DataPoint> areaDataPoints = new List<DataPoint>();
            for (int i = 0; i < b.Length; i++) {
                for (int j = i + 1; j < b.Length; j++) {
                    var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], getRow(A, j), b[j]);
                    if (coord != null) {
                        areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                    }
                }
                if (!isx2real) {
                    var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 0, 1 }, 0);
                    if (coord != null) {
                        areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                    }
                }
                else {
                    var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 0, 1 }, -10);
                    if (coord != null) {
                        areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                    }
                }
                if (!isx1real) {
                    var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 1, 0 }, 0);
                    if (coord != null) {
                        areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                    }
                }
                else {
                    var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 1, 0 }, -10);
                    if (coord != null) {
                        areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));

                    }
                }
            }

            double maxx1 = Double.NegativeInfinity;
            double maxx2 = Double.NegativeInfinity;
            double minx1 = Double.PositiveInfinity;
            double minx2 = Double.PositiveInfinity;
            foreach (var p in areaDataPoints) {
                maxx1 = Math.Max(p.X, maxx1);
                maxx2 = Math.Max(p.Y, maxx2);
                minx1 = Math.Min(p.X, minx1);
                minx2 = Math.Min(p.Y, minx2);
            }

            for (int i = 0; i < b.Length; i++) {
                var coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 0, 1 }, maxx2 + 10);
                if (coord != null) {
                    areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                }
                coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 0, 1 }, minx2 - 10);
                if (coord != null) {
                    areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                }
                coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 1, 0 }, maxx1 + 10);
                if (coord != null) {
                    areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                }
                coord = getIntersectionPoinOfTwoFun(getRow(A, i), b[i], new double[] { 1, 0 }, minx1 - 10);
                if (coord != null) {
                    areaDataPoints.Add(new DataPoint(coord.Item1, coord.Item2));
                }
            }
            if (!isx1real && !isx2real) {
                areaDataPoints.Add(new DataPoint(0, 0));
            }
            List<DataPoint> area = new List<DataPoint>();
            foreach (var dp in areaDataPoints) {
                bool leavePoint = true;
                for (int i = 0; i < b.Length && leavePoint; i++) {
                    if (A[i, 0] * dp.X + A[i, 1] * dp.Y > b[i]) {
                        leavePoint = false;
                    }
                    if (!isx1real && dp.X < 0 || !isx2real && dp.Y < 0) {
                        leavePoint = false;
                    }
                }
                if (leavePoint) {
                    area.Add(dp);
                }
            }

            List<Tuple<DataPoint, DataPoint>> completeGraph = new List<Tuple<DataPoint, DataPoint>>();
            for (int i = 0; i < area.Count; i++) {
                for (int j = i + 1; j < area.Count; j++) {
                    completeGraph.Add(Tuple.Create(area[i], area[j]));
                }
            }
            HashSet<Tuple<DataPoint, DataPoint>> finalGraph = new HashSet<Tuple<DataPoint, DataPoint>>(completeGraph);

            for (int i = 0; i < completeGraph.Count; i++) {
                for (int j = i + 1; j < completeGraph.Count; j++) {
                    var p = SimplexSolver.getLinesIntersection(completeGraph[i], completeGraph[j]);
                    if (!(Double.IsInfinity(p.Item1) || Double.IsInfinity(p.Item2) || Double.IsNaN(p.Item1) || Double.IsNaN(p.Item2))) {
                        if (p.Item1 < Math.Max(completeGraph[i].Item1.X, completeGraph[i].Item2.X) - 0.1 &&
                            p.Item1 > Math.Min(completeGraph[i].Item1.X, completeGraph[i].Item2.X) + 0.1 &&
                            p.Item2 < Math.Max(completeGraph[i].Item1.Y, completeGraph[i].Item2.Y) - 0.1 &&
                            p.Item2 > Math.Min(completeGraph[i].Item1.Y, completeGraph[i].Item2.Y) + 0.1) {
                            finalGraph.Remove(completeGraph[i]);
                            finalGraph.Remove(completeGraph[j]);
                        }
                    }
                }
            }

            List<DataPoint> result = new List<DataPoint>();
            List<Tuple<DataPoint, DataPoint>> graphList = new List<Tuple<DataPoint, DataPoint>>();
            foreach (var p in finalGraph) {
                graphList.Add(p);
            }
            DataPoint current = graphList[0].Item1;
            result.Add(graphList[0].Item1);
            while (finalGraph.Count > 0) {
                foreach (var fg in finalGraph) {
                    if (fg.Item1.Equals(current)) {
                        current = fg.Item2;
                        result.Add(fg.Item2);
                        finalGraph.Remove(fg);
                        break;
                    }
                    if (fg.Item2.Equals(current)) {
                        current = fg.Item1;
                        result.Add(fg.Item1);
                        finalGraph.Remove(fg);
                        break;
                    }
                }
            }

            return result;
            //area.OrderBy(point => point.X).ThenBy(point => point.Y).ToList();
        }

        AreaSeries createAreaSeries(List<DataPoint> points) {
            var areaSeries = new AreaSeries {
                Fill = OxyColor.FromArgb(100, 89, 92, 96),
                MarkerStrokeThickness = 1.0,
                StrokeThickness = 1,
                Smooth = false
            };

            foreach (var point in points) {
                areaSeries.Points.Add(point);
            }
            areaSeries.Points.Add(points[0]);
            return areaSeries;
        }

    }
}
