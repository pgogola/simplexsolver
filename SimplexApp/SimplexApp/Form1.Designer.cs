﻿namespace SimplexApp
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.Close = new System.Windows.Forms.Button();
            this.MatrixAadd = new System.Windows.Forms.Button();
            this.NumberOfColumnA = new System.Windows.Forms.TextBox();
            this.NumberOfRowA = new System.Windows.Forms.TextBox();
            this.TabPage = new System.Windows.Forms.TabControl();
            this.TabPageMatrixA = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.AcceptA = new System.Windows.Forms.Button();
            this.TabPageVectorB = new System.Windows.Forms.TabPage();
            this.AcceptB = new System.Windows.Forms.Button();
            this.NumberOfRowB = new System.Windows.Forms.TextBox();
            this.VectorBAdd = new System.Windows.Forms.Button();
            this.TabPageVecotrXb = new System.Windows.Forms.TabPage();
            this.AcceptXb = new System.Windows.Forms.Button();
            this.NumberOfRowXb = new System.Windows.Forms.TextBox();
            this.XbAdd = new System.Windows.Forms.Button();
            this.TabPageSimpleks = new System.Windows.Forms.TabPage();
            this.TabPageCountourLine = new System.Windows.Forms.TabPage();
            this.ContourLineShow = new System.Windows.Forms.Button();
            this.TabPage.SuspendLayout();
            this.TabPageMatrixA.SuspendLayout();
            this.TabPageVectorB.SuspendLayout();
            this.TabPageVecotrXb.SuspendLayout();
            this.TabPageCountourLine.SuspendLayout();
            this.SuspendLayout();
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(1173, 14);
            this.Close.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(112, 35);
            this.Close.TabIndex = 0;
            this.Close.Text = "Zamknij";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // MatrixAadd
            // 
            this.MatrixAadd.Location = new System.Drawing.Point(406, 61);
            this.MatrixAadd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MatrixAadd.Name = "MatrixAadd";
            this.MatrixAadd.Size = new System.Drawing.Size(112, 35);
            this.MatrixAadd.TabIndex = 1;
            this.MatrixAadd.Text = "Utwórz";
            this.MatrixAadd.UseVisualStyleBackColor = true;
            this.MatrixAadd.Click += new System.EventHandler(this.MatrixAadd_Click);
            // 
            // NumberOfColumnA
            // 
            this.NumberOfColumnA.Location = new System.Drawing.Point(34, 65);
            this.NumberOfColumnA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumberOfColumnA.Name = "NumberOfColumnA";
            this.NumberOfColumnA.Size = new System.Drawing.Size(148, 26);
            this.NumberOfColumnA.TabIndex = 2;
            // 
            // NumberOfRowA
            // 
            this.NumberOfRowA.Location = new System.Drawing.Point(210, 65);
            this.NumberOfRowA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumberOfRowA.Name = "NumberOfRowA";
            this.NumberOfRowA.Size = new System.Drawing.Size(148, 26);
            this.NumberOfRowA.TabIndex = 4;
            // 
            // TabPage
            // 
            this.TabPage.Controls.Add(this.TabPageMatrixA);
            this.TabPage.Controls.Add(this.TabPageVectorB);
            this.TabPage.Controls.Add(this.TabPageVecotrXb);
            this.TabPage.Controls.Add(this.TabPageSimpleks);
            this.TabPage.Controls.Add(this.TabPageCountourLine);
            this.TabPage.Location = new System.Drawing.Point(23, 32);
            this.TabPage.Name = "TabPage";
            this.TabPage.SelectedIndex = 0;
            this.TabPage.Size = new System.Drawing.Size(767, 568);
            this.TabPage.TabIndex = 5;
            // 
            // TabPageMatrixA
            // 
            this.TabPageMatrixA.Controls.Add(this.listView1);
            this.TabPageMatrixA.Controls.Add(this.AcceptA);
            this.TabPageMatrixA.Controls.Add(this.NumberOfRowA);
            this.TabPageMatrixA.Controls.Add(this.MatrixAadd);
            this.TabPageMatrixA.Controls.Add(this.NumberOfColumnA);
            this.TabPageMatrixA.Location = new System.Drawing.Point(4, 29);
            this.TabPageMatrixA.Name = "TabPageMatrixA";
            this.TabPageMatrixA.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageMatrixA.Size = new System.Drawing.Size(759, 535);
            this.TabPageMatrixA.TabIndex = 0;
            this.TabPageMatrixA.Text = "MatrixA";
            this.TabPageMatrixA.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(345, 275);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(121, 97);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // AcceptA
            // 
            this.AcceptA.Location = new System.Drawing.Point(406, 118);
            this.AcceptA.Name = "AcceptA";
            this.AcceptA.Size = new System.Drawing.Size(112, 36);
            this.AcceptA.TabIndex = 7;
            this.AcceptA.Text = "potwierdź";
            this.AcceptA.UseVisualStyleBackColor = true;
            this.AcceptA.Click += new System.EventHandler(this.AcceptA_Click);
            // 
            // TabPageVectorB
            // 
            this.TabPageVectorB.Controls.Add(this.AcceptB);
            this.TabPageVectorB.Controls.Add(this.NumberOfRowB);
            this.TabPageVectorB.Controls.Add(this.VectorBAdd);
            this.TabPageVectorB.Location = new System.Drawing.Point(4, 29);
            this.TabPageVectorB.Name = "TabPageVectorB";
            this.TabPageVectorB.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageVectorB.Size = new System.Drawing.Size(759, 535);
            this.TabPageVectorB.TabIndex = 1;
            this.TabPageVectorB.Text = "VectorB";
            this.TabPageVectorB.UseVisualStyleBackColor = true;
            // 
            // AcceptB
            // 
            this.AcceptB.Location = new System.Drawing.Point(227, 111);
            this.AcceptB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AcceptB.Name = "AcceptB";
            this.AcceptB.Size = new System.Drawing.Size(112, 35);
            this.AcceptB.TabIndex = 11;
            this.AcceptB.Text = "potwierdź";
            this.AcceptB.UseVisualStyleBackColor = true;
            this.AcceptB.Click += new System.EventHandler(this.AcceptB_Click);
            // 
            // NumberOfRowB
            // 
            this.NumberOfRowB.Location = new System.Drawing.Point(16, 62);
            this.NumberOfRowB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumberOfRowB.Name = "NumberOfRowB";
            this.NumberOfRowB.Size = new System.Drawing.Size(148, 26);
            this.NumberOfRowB.TabIndex = 10;
            // 
            // VectorBAdd
            // 
            this.VectorBAdd.Location = new System.Drawing.Point(227, 58);
            this.VectorBAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.VectorBAdd.Name = "VectorBAdd";
            this.VectorBAdd.Size = new System.Drawing.Size(112, 35);
            this.VectorBAdd.TabIndex = 7;
            this.VectorBAdd.Text = "Utwórz";
            this.VectorBAdd.UseVisualStyleBackColor = true;
            this.VectorBAdd.Click += new System.EventHandler(this.VectorBAdd_Click);
            // 
            // TabPageVecotrXb
            // 
            this.TabPageVecotrXb.Controls.Add(this.AcceptXb);
            this.TabPageVecotrXb.Controls.Add(this.NumberOfRowXb);
            this.TabPageVecotrXb.Controls.Add(this.XbAdd);
            this.TabPageVecotrXb.Location = new System.Drawing.Point(4, 29);
            this.TabPageVecotrXb.Name = "TabPageVecotrXb";
            this.TabPageVecotrXb.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageVecotrXb.Size = new System.Drawing.Size(759, 535);
            this.TabPageVecotrXb.TabIndex = 4;
            this.TabPageVecotrXb.Text = "Xb";
            this.TabPageVecotrXb.UseVisualStyleBackColor = true;
            // 
            // AcceptXb
            // 
            this.AcceptXb.Location = new System.Drawing.Point(247, 97);
            this.AcceptXb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AcceptXb.Name = "AcceptXb";
            this.AcceptXb.Size = new System.Drawing.Size(112, 35);
            this.AcceptXb.TabIndex = 14;
            this.AcceptXb.Text = "potwierdź";
            this.AcceptXb.UseVisualStyleBackColor = true;
            this.AcceptXb.Click += new System.EventHandler(this.AcceptXb_Click);
            // 
            // NumberOfRowXb
            // 
            this.NumberOfRowXb.Location = new System.Drawing.Point(36, 48);
            this.NumberOfRowXb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumberOfRowXb.Name = "NumberOfRowXb";
            this.NumberOfRowXb.Size = new System.Drawing.Size(148, 26);
            this.NumberOfRowXb.TabIndex = 13;
            // 
            // XbAdd
            // 
            this.XbAdd.Location = new System.Drawing.Point(247, 44);
            this.XbAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.XbAdd.Name = "XbAdd";
            this.XbAdd.Size = new System.Drawing.Size(112, 35);
            this.XbAdd.TabIndex = 12;
            this.XbAdd.Text = "Utwórz";
            this.XbAdd.UseVisualStyleBackColor = true;
            this.XbAdd.Click += new System.EventHandler(this.XbAdd_Click);
            // 
            // TabPageSimpleks
            // 
            this.TabPageSimpleks.Location = new System.Drawing.Point(4, 29);
            this.TabPageSimpleks.Name = "TabPageSimpleks";
            this.TabPageSimpleks.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageSimpleks.Size = new System.Drawing.Size(759, 535);
            this.TabPageSimpleks.TabIndex = 2;
            this.TabPageSimpleks.Text = "SimpleksTab";
            this.TabPageSimpleks.UseVisualStyleBackColor = true;
            // 
            // TabPageCountourLine
            // 
            this.TabPageCountourLine.Controls.Add(this.ContourLineShow);
            this.TabPageCountourLine.Location = new System.Drawing.Point(4, 29);
            this.TabPageCountourLine.Name = "TabPageCountourLine";
            this.TabPageCountourLine.Padding = new System.Windows.Forms.Padding(3);
            this.TabPageCountourLine.Size = new System.Drawing.Size(759, 535);
            this.TabPageCountourLine.TabIndex = 3;
            this.TabPageCountourLine.Text = "Warstwica";
            this.TabPageCountourLine.UseVisualStyleBackColor = true;
            // 
            // ContourLineShow
            // 
            this.ContourLineShow.Location = new System.Drawing.Point(544, 21);
            this.ContourLineShow.Name = "ContourLineShow";
            this.ContourLineShow.Size = new System.Drawing.Size(167, 26);
            this.ContourLineShow.TabIndex = 0;
            this.ContourLineShow.Text = "wyświetl";
            this.ContourLineShow.UseVisualStyleBackColor = true;
            this.ContourLineShow.Click += new System.EventHandler(this.ContourLineShow_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1298, 594);
            this.Controls.Add(this.TabPage);
            this.Controls.Add(this.Close);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.TabPage.ResumeLayout(false);
            this.TabPageMatrixA.ResumeLayout(false);
            this.TabPageMatrixA.PerformLayout();
            this.TabPageVectorB.ResumeLayout(false);
            this.TabPageVectorB.PerformLayout();
            this.TabPageVecotrXb.ResumeLayout(false);
            this.TabPageVecotrXb.PerformLayout();
            this.TabPageCountourLine.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button MatrixAadd;
        private System.Windows.Forms.TextBox NumberOfColumnA;
        private System.Windows.Forms.TextBox NumberOfRowA;
        private System.Windows.Forms.TabControl TabPage;
        private System.Windows.Forms.TabPage TabPageMatrixA;
        private System.Windows.Forms.TabPage TabPageVectorB;
        private System.Windows.Forms.TextBox NumberOfRowB;
        private System.Windows.Forms.Button VectorBAdd;
        private System.Windows.Forms.Button AcceptA;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button AcceptB;
        private System.Windows.Forms.TabPage TabPageSimpleks;
        private System.Windows.Forms.TabPage TabPageCountourLine;
        private System.Windows.Forms.TabPage TabPageVecotrXb;
        private System.Windows.Forms.Button AcceptXb;
        private System.Windows.Forms.TextBox NumberOfRowXb;
        private System.Windows.Forms.Button XbAdd;
        private System.Windows.Forms.Button ContourLineShow;
    }
}

