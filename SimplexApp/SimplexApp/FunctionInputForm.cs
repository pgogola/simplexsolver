﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SimplexApp {
    public partial class FunctionInputForm : UserControl {
        private int numberOfVariables;
        private List<TextBox> paramsList;
        public FunctionInputForm() {
            InitializeComponent();
        }

        public double[] getFunctionParams() {
            double[] c = new double[numberOfVariables];
            for (int i = 0; i < numberOfVariables; i++) {
                double result;
                if (!Double.TryParse(paramsList[i].Text.Replace(".", ","), out result)) {
                    if (paramsList[i].Text == "") {
                        throw new FormatException("Brak parametrów maksymalizowanej funkcji.\nNależy wypełnić wszystkie pola.");
                    }
                    else {
                        throw new FormatException("Nieprawidłowy format parametrów maksymalizowanej funckji.\nPoprawny format to liczba zmiennoprzecinkowa (dozwolony separator dziesiętny: \",\" lub \".\")");
                    }
                }
                c[i] = result;
            }
            return c;
        }

        public void setFunctionParams(double[] c) {
            for (int i = 0; i < c.Length; i++) {
                paramsList[i].Text = c[i].ToString();
            }
        }

        public void updateFunctionForm(int numberOfVariables) {
            this.numberOfVariables = numberOfVariables;
            functionLayoutPanel.Controls.Clear();
            paramsList = new List<TextBox>();
            for (int i = 0; i < numberOfVariables; i++) {
                TextBox xiParam = new TextBox();
                xiParam.Width = 50;
                xiParam.Font = new Font("Microsoft Sans Serif", 9f);
                functionLayoutPanel.Controls.Add(xiParam);
                paramsList.Add(xiParam);
                Label xiLabel = new Label();
                xiLabel.Text = "x" + (i + 1) + (i == numberOfVariables - 1 ? "" : " + ");
                xiLabel.Font = new Font("Microsoft Sans Serif", 9f);
                xiLabel.AutoSize = false;
                xiLabel.TextAlign = ContentAlignment.MiddleCenter;
                xiLabel.Dock = DockStyle.Fill;
                xiLabel.Size = new Size(xiLabel.PreferredWidth, xiLabel.PreferredHeight);
                functionLayoutPanel.Controls.Add(xiLabel);
            }
        }
    }
}
