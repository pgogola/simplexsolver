﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace SimplexApp
{
   

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int[] tabB;
        int[,] tabA;
        int[] tabXb;
        private object plot1;

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MatrixAadd_Click(object sender, EventArgs e)
        {
            int liczba_wierszyA = Convert.ToInt16(NumberOfColumnA.Text); //Konwersja wpisanej wartości do textboxa na int
            int liczba_kolumnA = Convert.ToInt16(NumberOfRowA.Text);

            for (int i = 0; i < liczba_wierszyA; i++)
            {
                for (int j = 0; j < liczba_kolumnA; j++)
                {
                    TextBox Atextbox = new TextBox();   //tworzymy nową kontrolkę - textbox, button, oraz label
                    Atextbox.Location = new System.Drawing.Point(39 + 26 * j, 110 + 26 * i);  //umieszczanie na ekranie danej kontrolki
                    Atextbox.Size = new System.Drawing.Size(20, 20); //określenie rozmiarów kontrolki
                    Atextbox.Name = "A_box" + j.ToString(); //przypisanie nazwy danej kontrolce
                    TabPageMatrixA.Controls.Add(Atextbox); //dodanie kontrolek do formularza (do kolekcji - tablicy)
                }

            }
        }
        private void VectorBAdd_Click(object sender, EventArgs e) // Vector B clic
        {
            int liczba_kolumnB = Convert.ToInt16(NumberOfRowB.Text); //Konwersja wpisanej wartości do textboxa na int
            for (int j = 0; j < liczba_kolumnB; j++)
            {
                TextBox Btextbox = new TextBox();
                Btextbox.Location = new System.Drawing.Point(39, 110 + 26 * j);
                Btextbox.Size = new System.Drawing.Size(20, 20);
                Btextbox.Name = "B_box" + j.ToString();
                TabPageVectorB.Controls.Add(Btextbox);


            }

        }
        private void XbAdd_Click(object sender, EventArgs e)
        {

            int liczba_kolumnXb = Convert.ToInt16(NumberOfRowXb.Text); //Konwersja wpisanej wartości do textboxa na int
            for (int j = 0; j < liczba_kolumnXb; j++)
            {
                TextBox Xbtextbox = new TextBox();
                Xbtextbox.Location = new System.Drawing.Point(39, 110 + 26 * j);
                Xbtextbox.Size = new System.Drawing.Size(20, 20);
                Xbtextbox.Name = "Xb_box" + j.ToString();
                TabPageVecotrXb.Controls.Add(Xbtextbox);


            }


        }

        private void AcceptA_Click(object sender, EventArgs e)
        {
            int liczba_wierszyA = Convert.ToInt16(NumberOfColumnA.Text); //Konwersja wpisanej wartości do textboxa na int
            int liczba_kolumnA = Convert.ToInt16(NumberOfRowA.Text);
            ListViewItem lvl = new ListViewItem();
            for (int i = 0; i < liczba_wierszyA; i++)
            {
                for (int j = 0; j < liczba_kolumnA; j++)
                {
                    tabA[i, j] = Convert.ToInt16("A_box" + j.ToString());

                    //listView1.Items.Add(Convert.ToString(tabA[i, j])); //pomocnicze wyswietlanie 
                }
            }
            

        }

        private void AcceptB_Click(object sender, EventArgs e)
        {
            //Konwersja wpisanej wartości do textboxa na int
            int liczba_kolumnB = Convert.ToInt16(NumberOfRowB.Text);
            ListViewItem lvl = new ListViewItem();

            for (int j = 0; j < liczba_kolumnB; j++)
            {
                tabB[j] = Convert.ToInt16("B_box" + j.ToString());

                //listView1.Items.Add(Convert.ToString(tabA[i, j])); //pomocnicze wyswietlanie 
            }


        }

        private void AcceptXb_Click(object sender, EventArgs e)
        {
            //Konwersja wpisanej wartości do textboxa na int
            int liczba_kolumnXb = Convert.ToInt16(NumberOfRowA.Text);

            for (int j = 0; j < liczba_kolumnXb; j++)
            {
                tabXb[j] = Convert.ToInt16("Xb_box" + j.ToString());

                //listView1.Items.Add(Convert.ToString(tabA[i, j])); //pomocnicze wyswietlanie 
            }
        }

        private void ContourLineShow_Click(object sender, EventArgs e)
        {
            //Create Plotview object
            PlotView myPlot = new PlotView();

            //Create Plotmodel object
            var areaSeries1 = new AreaSeries();

            var myModel = new PlotModel { Title = "Warswtica" };
            // myModel.Series.Add(new FunctionSeries(0, 10, 0.1, "sin(x)"));
            areaSeries1.Points2.Add(new DataPoint(0, -5.04135905692417));

            //Assign PlotModel to PlotView
            myPlot.Model = myModel;

            //Set up plot for display
            myPlot.Dock = System.Windows.Forms.DockStyle.Bottom;
            myPlot.Location = new System.Drawing.Point(0, 0);
            myPlot.Size = new System.Drawing.Size(300, 300);
            myPlot.TabIndex = 0;
            
            //Add plot control to form
            TabPageCountourLine.Controls.Add(myPlot);
           

        }
    }



    /**
     *   private void Nbutton_Click(object sender, EventArgs e)
        {
            //odczytanie numeru przycisku który spowodował wywołanie tego zdarzenia
            string nr_kontrolki = (sender as Button).Name.Remove(0, 9);

            //tworzymy nowe obiekty - dokładnie uchwyty do istniejących obiektów (poprzez wyszukanie FIND)
            TextBox ptextbox = (TextBox)this.Controls.Find("d_box_" + nr_kontrolki, false).First();
            Label plabel = this.Controls.Find("d_label_" + nr_kontrolki, false).First() as Label;

            //przepisanie zawartości
            plabel.Text = ptextbox.Text;

            //usunięcie przycisku który spowodował to zdarzenie - usunięcie z kolekcji
            Controls.Remove(sender as Button);

        }
     */



}
