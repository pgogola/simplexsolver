﻿using System.Diagnostics;
using System.Linq;

namespace SimplexApp {
    class ExamplePL {
        private string description;
        public double[,] A;
        public double[] b;
        public double[] c;
        public string[] realVariables;
        private string[] variables;

        public ExamplePL(double[,] A, double[] b, double[] c, string[] realVariables, string[] variables, string description = "") {
            Debug.Assert(variables.Length == c.Length, "");
            Debug.Assert(realVariables.Length <= variables.Length);
            Debug.Assert(A.GetLength(0) == b.Length);
            Debug.Assert(A.GetLength(1) == c.Length);
            this.A = A;
            this.b = b;
            this.c = c;
            this.realVariables = realVariables;
            this.variables = variables;
            this.description = description;
        }

        public string getFunction() {
            string fun = "f(x) = ";
            for (int i = 0; i < c.Length; i++) {
                fun += c[i] + "\u22C5" + variables[i] + (i < c.Length - 1 ? " + " : "");
            }
            return fun;
        }

        public string getDescription() {
            return description;
        }

        public string getRealVariablesString() {
            string res = "";
            for (int i = 0; i < realVariables.Length; i++) {
                res += realVariables[i] + (i < realVariables.Length - 1 ? ", " : "");
            }
            if (res != "") {
                res += " \u2208 R";
            }
            return res;
        }

        public string getNonRealVariablesString() {
            string res = "";
            for (int i = 0; i < variables.Length; i++) {
                if (!realVariables.Contains(variables[i])) {
                    res += variables[i] + (i < variables.Length - 1 ? ", " : "");
                }
            }
            if (res != "") {
                res += " \u2265 0";
            }
            return res;
        }

        public int getNumberOfContraints() {
            return b.Length;
        }

        public int getNumberOfVariables() {
            return c.Length;
        }




    }
}
