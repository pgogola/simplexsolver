﻿using System.Windows.Forms;

namespace SimplexApp {
    partial class GUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.contraintQtyLabel = new System.Windows.Forms.Label();
            this.variableAmountLabel = new System.Windows.Forms.Label();
            this.constraintsAmountComboBox = new System.Windows.Forms.ComboBox();
            this.variablesAmountComboBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.windowsTabControl = new System.Windows.Forms.TabControl();
            this.inputDataTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.selectRealVariablesListBox = new System.Windows.Forms.CheckedListBox();
            this.selectRealVariableLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.greaterXContraintLabel = new System.Windows.Forms.Label();
            this.realVariablesLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.examplePLAcceptButton = new System.Windows.Forms.Button();
            this.examplePLComboBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.inputDataTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.solveButton = new System.Windows.Forms.Button();
            this.solutionTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.showResultLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.resultIndexBox = new System.Windows.Forms.TextBox();
            this.resultFirstButton = new System.Windows.Forms.Button();
            this.resultPreviousButton = new System.Windows.Forms.Button();
            this.resultNextButton = new System.Windows.Forms.Button();
            this.resultLastButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.currentResultLabel = new System.Windows.Forms.Label();
            this.functionRememberLabel = new System.Windows.Forms.Label();
            this.functionInputForm = new SimplexApp.FunctionInputForm();
            this.matrixAForm = new SimplexApp.MatrixForm();
            this.matrixXForm = new SimplexApp.MatrixForm();
            this.vectorbForm = new SimplexApp.MatrixForm();
            this.resultSimplexTableForm = new SimplexApp.SimplexTableForm();
            this.plotForm = new SimplexApp.PlotForm();
            this.tableLayoutPanel1.SuspendLayout();
            this.windowsTabControl.SuspendLayout();
            this.inputDataTabPage.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.inputDataTableLayout.SuspendLayout();
            this.solutionTabPage.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.showResultLayoutPanel.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.SuspendLayout();
            // 
            // contraintQtyLabel
            // 
            this.contraintQtyLabel.AutoSize = true;
            this.contraintQtyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contraintQtyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.contraintQtyLabel.Location = new System.Drawing.Point(3, 0);
            this.contraintQtyLabel.Name = "contraintQtyLabel";
            this.contraintQtyLabel.Size = new System.Drawing.Size(190, 48);
            this.contraintQtyLabel.TabIndex = 3;
            this.contraintQtyLabel.Text = "Liczba ograniczeń (m):";
            this.contraintQtyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // variableAmountLabel
            // 
            this.variableAmountLabel.AutoSize = true;
            this.variableAmountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variableAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.variableAmountLabel.Location = new System.Drawing.Point(3, 48);
            this.variableAmountLabel.Name = "variableAmountLabel";
            this.variableAmountLabel.Size = new System.Drawing.Size(190, 49);
            this.variableAmountLabel.TabIndex = 4;
            this.variableAmountLabel.Text = "Liczba zmiennych (n):";
            this.variableAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // constraintsAmountComboBox
            // 
            this.constraintsAmountComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.constraintsAmountComboBox.FormattingEnabled = true;
            this.constraintsAmountComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.constraintsAmountComboBox.Location = new System.Drawing.Point(199, 10);
            this.constraintsAmountComboBox.Name = "constraintsAmountComboBox";
            this.constraintsAmountComboBox.Size = new System.Drawing.Size(191, 28);
            this.constraintsAmountComboBox.TabIndex = 5;
            this.constraintsAmountComboBox.SelectedIndexChanged += new System.EventHandler(this.constraintsAmountComboBox_SelectedIndexChanged);
            // 
            // variablesAmountComboBox
            // 
            this.variablesAmountComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.variablesAmountComboBox.FormattingEnabled = true;
            this.variablesAmountComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.variablesAmountComboBox.Location = new System.Drawing.Point(199, 58);
            this.variablesAmountComboBox.Name = "variablesAmountComboBox";
            this.variablesAmountComboBox.Size = new System.Drawing.Size(191, 28);
            this.variablesAmountComboBox.TabIndex = 6;
            this.variablesAmountComboBox.SelectedIndexChanged += new System.EventHandler(this.variablesAmountComboBox_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.contraintQtyLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.constraintsAmountComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.variableAmountLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.variablesAmountComboBox, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(393, 97);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(499, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "<=";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // windowsTabControl
            // 
            this.windowsTabControl.Controls.Add(this.inputDataTabPage);
            this.windowsTabControl.Controls.Add(this.solutionTabPage);
            this.windowsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.windowsTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.windowsTabControl.Location = new System.Drawing.Point(0, 0);
            this.windowsTabControl.Name = "windowsTabControl";
            this.windowsTabControl.SelectedIndex = 0;
            this.windowsTabControl.Size = new System.Drawing.Size(1163, 587);
            this.windowsTabControl.TabIndex = 10;
            // 
            // inputDataTabPage
            // 
            this.inputDataTabPage.BackColor = System.Drawing.Color.PaleGreen;
            this.inputDataTabPage.Controls.Add(this.tableLayoutPanel4);
            this.inputDataTabPage.Location = new System.Drawing.Point(4, 29);
            this.inputDataTabPage.Name = "inputDataTabPage";
            this.inputDataTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.inputDataTabPage.Size = new System.Drawing.Size(1155, 554);
            this.inputDataTabPage.TabIndex = 0;
            this.inputDataTabPage.Text = "Dane wejściowe";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel15, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.12568F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.87432F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1149, 548);
            this.tableLayoutPanel4.TabIndex = 16;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel13, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(408, 6);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(735, 97);
            this.tableLayoutPanel5.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(729, 48);
            this.label1.TabIndex = 16;
            this.label1.Text = "Maksymalizowana funckja celu x0 = max(f(x))";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.functionInputForm, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 51);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(729, 43);
            this.tableLayoutPanel13.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 43);
            this.label6.TabIndex = 16;
            this.label6.Text = "f(x) = ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(6, 112);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(393, 430);
            this.tableLayoutPanel7.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.selectRealVariablesListBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.selectRealVariableLabel, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.16767F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.83234F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(387, 209);
            this.tableLayoutPanel2.TabIndex = 12;
            // 
            // selectRealVariablesListBox
            // 
            this.selectRealVariablesListBox.BackColor = System.Drawing.Color.PaleGreen;
            this.selectRealVariablesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectRealVariablesListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectRealVariablesListBox.FormattingEnabled = true;
            this.selectRealVariablesListBox.Location = new System.Drawing.Point(3, 36);
            this.selectRealVariablesListBox.Name = "selectRealVariablesListBox";
            this.selectRealVariablesListBox.Size = new System.Drawing.Size(381, 170);
            this.selectRealVariablesListBox.TabIndex = 10;
            this.selectRealVariablesListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.selectRealVariablesListBox_ItemCheck);
            // 
            // selectRealVariableLabel
            // 
            this.selectRealVariableLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectRealVariableLabel.AutoSize = true;
            this.selectRealVariableLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.selectRealVariableLabel.Location = new System.Drawing.Point(3, 0);
            this.selectRealVariableLabel.Name = "selectRealVariableLabel";
            this.selectRealVariableLabel.Size = new System.Drawing.Size(381, 33);
            this.selectRealVariableLabel.TabIndex = 11;
            this.selectRealVariableLabel.Text = "Wybór zmiennych rzeczywistych";
            this.selectRealVariableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 218);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(387, 209);
            this.tableLayoutPanel9.TabIndex = 13;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.greaterXContraintLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.realVariablesLabel, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(381, 59);
            this.tableLayoutPanel3.TabIndex = 14;
            // 
            // greaterXContraintLabel
            // 
            this.greaterXContraintLabel.AutoSize = true;
            this.greaterXContraintLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.greaterXContraintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.greaterXContraintLabel.Location = new System.Drawing.Point(3, 0);
            this.greaterXContraintLabel.Name = "greaterXContraintLabel";
            this.greaterXContraintLabel.Size = new System.Drawing.Size(375, 29);
            this.greaterXContraintLabel.TabIndex = 13;
            this.greaterXContraintLabel.Text = "x >= 0";
            this.greaterXContraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // realVariablesLabel
            // 
            this.realVariablesLabel.AutoSize = true;
            this.realVariablesLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.realVariablesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.realVariablesLabel.Location = new System.Drawing.Point(3, 29);
            this.realVariablesLabel.Name = "realVariablesLabel";
            this.realVariablesLabel.Size = new System.Drawing.Size(375, 30);
            this.realVariablesLabel.TabIndex = 14;
            this.realVariablesLabel.Text = "x in R";
            this.realVariablesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.examplePLAcceptButton, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.examplePLComboBox, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 107);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.0367F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.9633F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(381, 99);
            this.tableLayoutPanel6.TabIndex = 17;
            // 
            // examplePLAcceptButton
            // 
            this.examplePLAcceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.examplePLAcceptButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.examplePLAcceptButton.Location = new System.Drawing.Point(3, 46);
            this.examplePLAcceptButton.Name = "examplePLAcceptButton";
            this.examplePLAcceptButton.Size = new System.Drawing.Size(375, 50);
            this.examplePLAcceptButton.TabIndex = 1;
            this.examplePLAcceptButton.Text = "Załaduj przykład";
            this.examplePLAcceptButton.UseVisualStyleBackColor = true;
            this.examplePLAcceptButton.Click += new System.EventHandler(this.examplePLAcceptButton_Click);
            // 
            // examplePLComboBox
            // 
            this.examplePLComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.examplePLComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.examplePLComboBox.FormattingEnabled = true;
            this.examplePLComboBox.Location = new System.Drawing.Point(3, 7);
            this.examplePLComboBox.Name = "examplePLComboBox";
            this.examplePLComboBox.Size = new System.Drawing.Size(375, 28);
            this.examplePLComboBox.TabIndex = 0;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.inputDataTableLayout, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.solveButton, 0, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(408, 112);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.04651F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.95349F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(735, 430);
            this.tableLayoutPanel15.TabIndex = 22;
            // 
            // inputDataTableLayout
            // 
            this.inputDataTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputDataTableLayout.AutoSize = true;
            this.inputDataTableLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.inputDataTableLayout.ColumnCount = 4;
            this.inputDataTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.inputDataTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.inputDataTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.inputDataTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.inputDataTableLayout.Controls.Add(this.matrixAForm, 0, 1);
            this.inputDataTableLayout.Controls.Add(this.matrixXForm, 1, 1);
            this.inputDataTableLayout.Controls.Add(this.label2, 2, 1);
            this.inputDataTableLayout.Controls.Add(this.vectorbForm, 3, 1);
            this.inputDataTableLayout.Controls.Add(this.label3, 0, 0);
            this.inputDataTableLayout.Controls.Add(this.label4, 1, 0);
            this.inputDataTableLayout.Controls.Add(this.label5, 3, 0);
            this.inputDataTableLayout.Location = new System.Drawing.Point(10, 10);
            this.inputDataTableLayout.Margin = new System.Windows.Forms.Padding(10);
            this.inputDataTableLayout.Name = "inputDataTableLayout";
            this.inputDataTableLayout.Padding = new System.Windows.Forms.Padding(10);
            this.inputDataTableLayout.RowCount = 2;
            this.inputDataTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.inputDataTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.inputDataTableLayout.Size = new System.Drawing.Size(715, 64);
            this.inputDataTableLayout.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(13, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(341, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "A";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(360, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 24);
            this.label4.TabIndex = 12;
            this.label4.Text = "x";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(568, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "b";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // solveButton
            // 
            this.solveButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.solveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.solveButton.Location = new System.Drawing.Point(3, 372);
            this.solveButton.Name = "solveButton";
            this.solveButton.Size = new System.Drawing.Size(729, 55);
            this.solveButton.TabIndex = 1;
            this.solveButton.Text = "Optymalizuj";
            this.solveButton.UseVisualStyleBackColor = true;
            this.solveButton.Click += new System.EventHandler(this.solveButton_Click);
            // 
            // solutionTabPage
            // 
            this.solutionTabPage.Controls.Add(this.tableLayoutPanel8);
            this.solutionTabPage.Location = new System.Drawing.Point(4, 29);
            this.solutionTabPage.Name = "solutionTabPage";
            this.solutionTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.solutionTabPage.Size = new System.Drawing.Size(1155, 554);
            this.solutionTabPage.TabIndex = 1;
            this.solutionTabPage.Text = "Rozwiązanie";
            this.solutionTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.PaleGreen;
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.showResultLayoutPanel, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.functionRememberLabel, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.338028F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.66197F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1149, 548);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // showResultLayoutPanel
            // 
            this.showResultLayoutPanel.ColumnCount = 2;
            this.showResultLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.showResultLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.showResultLayoutPanel.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.showResultLayoutPanel.Controls.Add(this.plotForm, 1, 0);
            this.showResultLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.showResultLayoutPanel.Location = new System.Drawing.Point(3, 37);
            this.showResultLayoutPanel.Name = "showResultLayoutPanel";
            this.showResultLayoutPanel.RowCount = 1;
            this.showResultLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.showResultLayoutPanel.Size = new System.Drawing.Size(1143, 508);
            this.showResultLayoutPanel.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel16, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(565, 502);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 5;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12.Controls.Add(this.resultIndexBox, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.resultFirstButton, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.resultPreviousButton, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.resultNextButton, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.resultLastButton, 4, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(559, 38);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // resultIndexBox
            // 
            this.resultIndexBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.resultIndexBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultIndexBox.Location = new System.Drawing.Point(225, 5);
            this.resultIndexBox.Name = "resultIndexBox";
            this.resultIndexBox.Size = new System.Drawing.Size(105, 27);
            this.resultIndexBox.TabIndex = 0;
            this.resultIndexBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.resultIndexBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.resultIndexBox_TextChanged);
            // 
            // resultFirstButton
            // 
            this.resultFirstButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultFirstButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultFirstButton.Location = new System.Drawing.Point(3, 3);
            this.resultFirstButton.Name = "resultFirstButton";
            this.resultFirstButton.Size = new System.Drawing.Size(105, 32);
            this.resultFirstButton.TabIndex = 1;
            this.resultFirstButton.Text = "Początek";
            this.resultFirstButton.UseVisualStyleBackColor = true;
            this.resultFirstButton.Click += new System.EventHandler(this.resultFirstButton_Click);
            // 
            // resultPreviousButton
            // 
            this.resultPreviousButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultPreviousButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultPreviousButton.Location = new System.Drawing.Point(114, 3);
            this.resultPreviousButton.Name = "resultPreviousButton";
            this.resultPreviousButton.Size = new System.Drawing.Size(105, 32);
            this.resultPreviousButton.TabIndex = 2;
            this.resultPreviousButton.Text = "Cofnij";
            this.resultPreviousButton.UseVisualStyleBackColor = true;
            this.resultPreviousButton.Click += new System.EventHandler(this.resultPreviousButton_Click);
            // 
            // resultNextButton
            // 
            this.resultNextButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultNextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultNextButton.Location = new System.Drawing.Point(336, 3);
            this.resultNextButton.Name = "resultNextButton";
            this.resultNextButton.Size = new System.Drawing.Size(105, 32);
            this.resultNextButton.TabIndex = 3;
            this.resultNextButton.Text = "Następny";
            this.resultNextButton.UseVisualStyleBackColor = true;
            this.resultNextButton.Click += new System.EventHandler(this.resultNextButton_Click);
            // 
            // resultLastButton
            // 
            this.resultLastButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultLastButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.resultLastButton.Location = new System.Drawing.Point(447, 3);
            this.resultLastButton.Name = "resultLastButton";
            this.resultLastButton.Size = new System.Drawing.Size(109, 32);
            this.resultLastButton.TabIndex = 4;
            this.resultLastButton.Text = "Koniec";
            this.resultLastButton.UseVisualStyleBackColor = true;
            this.resultLastButton.Click += new System.EventHandler(this.resultLastButton_Click);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.resultSimplexTableForm, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.currentResultLabel, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 47);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel16.Size = new System.Drawing.Size(559, 452);
            this.tableLayoutPanel16.TabIndex = 1;
            // 
            // currentResultLabel
            // 
            this.currentResultLabel.AutoSize = true;
            this.currentResultLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.currentResultLabel.Location = new System.Drawing.Point(3, 0);
            this.currentResultLabel.Name = "currentResultLabel";
            this.currentResultLabel.Size = new System.Drawing.Size(553, 20);
            this.currentResultLabel.TabIndex = 2;
            this.currentResultLabel.Text = "label7";
            this.currentResultLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // functionRememberLabel
            // 
            this.functionRememberLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionRememberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.functionRememberLabel.Location = new System.Drawing.Point(3, 0);
            this.functionRememberLabel.Name = "functionRememberLabel";
            this.functionRememberLabel.Size = new System.Drawing.Size(1143, 34);
            this.functionRememberLabel.TabIndex = 1;
            this.functionRememberLabel.Text = "label7";
            this.functionRememberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // functionInputForm
            // 
            this.functionInputForm.BackColor = System.Drawing.Color.Transparent;
            this.functionInputForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionInputForm.Location = new System.Drawing.Point(70, 3);
            this.functionInputForm.Name = "functionInputForm";
            this.functionInputForm.Size = new System.Drawing.Size(656, 37);
            this.functionInputForm.TabIndex = 15;
            // 
            // matrixAForm
            // 
            this.matrixAForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixAForm.AutoSize = true;
            this.matrixAForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.matrixAForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.matrixAForm.Location = new System.Drawing.Point(13, 37);
            this.matrixAForm.Name = "matrixAForm";
            this.matrixAForm.Size = new System.Drawing.Size(341, 10);
            this.matrixAForm.TabIndex = 7;
            // 
            // matrixXForm
            // 
            this.matrixXForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixXForm.AutoSize = true;
            this.matrixXForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.matrixXForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.matrixXForm.Location = new System.Drawing.Point(360, 37);
            this.matrixXForm.Name = "matrixXForm";
            this.matrixXForm.Size = new System.Drawing.Size(133, 10);
            this.matrixXForm.TabIndex = 9;
            // 
            // vectorbForm
            // 
            this.vectorbForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vectorbForm.AutoSize = true;
            this.vectorbForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.vectorbForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vectorbForm.Location = new System.Drawing.Point(568, 37);
            this.vectorbForm.Name = "vectorbForm";
            this.vectorbForm.Size = new System.Drawing.Size(134, 10);
            this.vectorbForm.TabIndex = 8;
            // 
            // resultSimplexTableForm
            // 
            this.resultSimplexTableForm.AutoSize = true;
            this.resultSimplexTableForm.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.resultSimplexTableForm.Location = new System.Drawing.Point(3, 23);
            this.resultSimplexTableForm.Name = "resultSimplexTableForm";
            this.resultSimplexTableForm.Size = new System.Drawing.Size(0, 0);
            this.resultSimplexTableForm.TabIndex = 1;
            // 
            // plotForm
            // 
            this.plotForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotForm.Location = new System.Drawing.Point(574, 3);
            this.plotForm.Name = "plotForm";
            this.plotForm.Size = new System.Drawing.Size(566, 502);
            this.plotForm.TabIndex = 1;
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 587);
            this.Controls.Add(this.windowsTabControl);
            this.Name = "GUI";
            this.Text = "GUI";
            this.Load += new System.EventHandler(this.GUI_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.windowsTabControl.ResumeLayout(false);
            this.inputDataTabPage.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.inputDataTableLayout.ResumeLayout(false);
            this.inputDataTableLayout.PerformLayout();
            this.solutionTabPage.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.showResultLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label contraintQtyLabel;
        private System.Windows.Forms.Label variableAmountLabel;
        private System.Windows.Forms.ComboBox constraintsAmountComboBox;
        private System.Windows.Forms.ComboBox variablesAmountComboBox;
        private MatrixForm matrixAForm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MatrixForm vectorbForm;
        private MatrixForm matrixXForm;
        private System.Windows.Forms.TabControl windowsTabControl;
        private System.Windows.Forms.TabPage inputDataTabPage;
        private System.Windows.Forms.TabPage solutionTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label selectRealVariableLabel;
        private System.Windows.Forms.CheckedListBox selectRealVariablesListBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label greaterXContraintLabel;
        private System.Windows.Forms.Label realVariablesLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private FunctionInputForm functionInputForm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel inputDataTableLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button solveButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel showResultLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TextBox resultIndexBox;
        private System.Windows.Forms.Button resultFirstButton;
        private System.Windows.Forms.Button resultPreviousButton;
        private System.Windows.Forms.Button resultNextButton;
        private System.Windows.Forms.Button resultLastButton;
        private SimplexTableForm resultSimplexTableForm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.ComboBox examplePLComboBox;
        private TableLayoutPanel tableLayoutPanel16;
        private Label currentResultLabel;
        private Label functionRememberLabel;
        private PlotForm plotForm;
        private Button examplePLAcceptButton;
    }
}