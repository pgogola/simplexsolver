﻿using SimplexApp.simplex_algorithm;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SimplexApp {
    public partial class SimplexTableForm : UserControl {
        private int rows;
        private int cols;
        private string doubleFormat = "0.0000";
        public SimplexTableForm() {
            InitializeComponent();
        }

        private void SimplexTableForm_Load(object sender, EventArgs e) { 
            simplexTableFormTableLayoutPanel.CellPaint += new TableLayoutCellPaintEventHandler(tableLayoutPanel1_CellPaint);
        }

        void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e) {
            if (e.Row == 0) {
                Graphics g = e.Graphics;
                Rectangle r = e.CellBounds;
                g.FillRectangle(new SolidBrush(Color.FromArgb(83, 249, 83)), r);
            }else if(e.Column == 0) {
                Graphics g = e.Graphics;
                Rectangle r = e.CellBounds;
                g.FillRectangle(new SolidBrush(Color.FromArgb(83, 249, 83)), r);
            }
        }

        public void updateSimplexTable(SimplexTable simplexTable) {
            for(int r = 0; r<simplexTable.tableRows; r++) {
                for (int c = 0; c < simplexTable.tableCols; c++) {
                    Console.Write(simplexTable.getTableValue(r, c) + " ");
                }
                Console.WriteLine();
            }

            Func<string, Label> buildLabel = (text) => {
                Label llabel = new Label();
                llabel.Width = 40;
                llabel.AutoSize = false;
                llabel.TextAlign = ContentAlignment.MiddleCenter;
                llabel.Dock = DockStyle.Fill;
                llabel.Text = text;
                llabel.Font = new Font("Microsoft Sans Serif", 10f);
                llabel.BackColor = Color.Transparent;
                llabel.Size = new Size(llabel.PreferredWidth, llabel.PreferredHeight);
                return llabel;
            };

            simplexTableFormTableLayoutPanel.SuspendLayout();
            rows = simplexTable.tableRows + 1;
            cols = simplexTable.tableCols + 1;
            simplexTableFormTableLayoutPanel.RowCount = rows;
            simplexTableFormTableLayoutPanel.ColumnCount = cols;
            simplexTableFormTableLayoutPanel.ColumnStyles.Clear();
            simplexTableFormTableLayoutPanel.RowStyles.Clear();
            simplexTableFormTableLayoutPanel.Controls.Clear();
            foreach (var nonBase in simplexTable.nonBaseVariables) {
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel("-" + nonBase.Key), nonBase.Value + 1, 0);
                for (int r = 1; r < rows; r++) {
                    simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(simplexTable.getTableValue(r - 1, nonBase.Value).ToString(doubleFormat)), nonBase.Value + 1, r);
                }
            }
            foreach (var baseV in simplexTable.baseVariables) {
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(baseV.Key), 0, baseV.Value + 1);
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(simplexTable.getTableValue(baseV.Value, 0).ToString(doubleFormat)), 1, baseV.Value + 1);

            }
            if (simplexTable.phase == SimplexTable.Phase.FIRST_PHASE) {
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel("z0"), 0, 1);
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(simplexTable.getTableValue(0, 0).ToString(doubleFormat)), 1, 1);
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel("x0"), 0, 2);
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(simplexTable.getTableValue(1, 0).ToString(doubleFormat)), 1, 2);
            }
            else {
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel("x0"), 0, 1);
                simplexTableFormTableLayoutPanel.Controls.Add(buildLabel(simplexTable.getTableValue(0, 0).ToString(doubleFormat)), 1, 1);
            }
            simplexTableFormTableLayoutPanel.ResumeLayout();
        }
    }
}
