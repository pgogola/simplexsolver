﻿namespace SimplexApp {
    partial class SimplexTableForm {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować 
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent() {
            this.simplexTableFormTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // simplexTableFormTableLayoutPanel
            // 
            this.simplexTableFormTableLayoutPanel.AutoSize = true;
            this.simplexTableFormTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.simplexTableFormTableLayoutPanel.ColumnCount = 1;
            this.simplexTableFormTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.simplexTableFormTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.simplexTableFormTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simplexTableFormTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.simplexTableFormTableLayoutPanel.Name = "simplexTableFormTableLayoutPanel";
            this.simplexTableFormTableLayoutPanel.RowCount = 1;
            this.simplexTableFormTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.simplexTableFormTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.simplexTableFormTableLayoutPanel.Size = new System.Drawing.Size(360, 245);
            this.simplexTableFormTableLayoutPanel.TabIndex = 0;
            // 
            // SimplexTableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.simplexTableFormTableLayoutPanel);
            this.Name = "SimplexTableForm";
            this.Size = new System.Drawing.Size(360, 245);
            this.Load += new System.EventHandler(this.SimplexTableForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel simplexTableFormTableLayoutPanel;
    }
}
