﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplexApp {
    public partial class MatrixForm : UserControl {
        private int rows;
        private int cols;
       
        public MatrixForm() {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        public void updateMatrixSize(int rows, int cols) {
            this.DoubleBuffered = true;
            this.rows = rows;
            this.cols = cols;
            this.SuspendLayout();

            matrixTableLayout.SuspendLayout();
            matrixTableLayout.RowCount = rows;
            matrixTableLayout.ColumnCount = cols;
            matrixTableLayout.Controls.Clear();
            for (int r = 0; r<rows; r++) {
                for(int c = 0; c<cols; c++) {
                    TextBox tb = new TextBox();
                    tb.Width = 40;
                    //tb.Anchor = AnchorStyles.Left;
                    //tb.Dock = DockStyle.Fill;
                    tb.AutoSize = true;
                
                    tb.Font = new Font("Microsoft Sans Serif", 9f);
                    matrixTableLayout.Controls.Add(tb, c, r);
                }
            }
            matrixTableLayout.ResumeLayout();
            this.ResumeLayout();
      
        }

        public void updateMatrixSize(int rows, int cols, String[,] values) {
            this.rows = rows;
            this.cols = cols;
            matrixTableLayout.SuspendLayout();
            matrixTableLayout.Controls.Clear();
            matrixTableLayout.RowCount = rows;
            matrixTableLayout.ColumnCount = cols;
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < cols; c++) {
                    Label l = new Label();
                    l.Width = 40;
                    l.AutoSize = false;
                    l.TextAlign = ContentAlignment.MiddleCenter;
                    l.Font = new Font("Microsoft Sans Serif", 9f);
                    l.Dock = DockStyle.Fill;
                    l.Text = values[r, c];
                    matrixTableLayout.Controls.Add(l, c, r);
                }
            }

            matrixTableLayout.ResumeLayout();
        }

        public String getElementAt(int row, int col) {
            return matrixTableLayout.GetControlFromPosition(col, row).Text;
        }

        public void setElementAt(string val, int row, int col) {
            matrixTableLayout.GetControlFromPosition(col, row).Text = val;
        }
    }
}
